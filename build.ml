#!/usr/bin/env ocaml

#use "ocb.ml"

open Ocb

let ocb =
  { ocb_bin = "ocamlbuild";
    ocb_flags = ["-use-ocamlfind"; "-use-menhir"];
    ocb_default_args = ["tests.native"; "stlpi.native"; "mksignal.native"];
    ocb_aliases = [
      ("byte", ["tests.byte"; "stlpi.byte"; "mksignal.byte"]);
      ("clean", ["-clean"]);
      ("doc", ["doc.docdir/index.html"]);
    ] }

let () =
  match wrap_ocb ocb with
  | None -> ()
  | Some code -> exit code
