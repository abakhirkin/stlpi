
let int_in a b =
  a + Random.int (b - a)

let float_in a b =
  a +. Random.float (b -. a)
