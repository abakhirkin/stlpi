
module type T = sig
  type t

  val zero: t

  val one: t

  val minus_one: t

  val inf: t

  val minus_inf: t

  val of_int: int -> t
  
  val neg: t -> t

  val add: t -> t -> t

  val mul: t -> t -> t

  val equal: t -> t -> bool

  val leq: t -> t -> bool

  val lt: t -> t -> bool

  val geq: t -> t -> bool
  
  val gt: t -> t -> bool

  val compare: t -> t -> int

  val max: t -> t -> t

  val format: Format.formatter -> t -> unit

  val to_string: t -> string
end

module Float = struct
  type t = float

  let zero = 0.0

  let one = 1.0

  let minus_one = (-. 1.0)

  let inf = infinity

  let minus_inf = neg_infinity

  let of_int i = float_of_int i
  
  let neg f = (-. f)

  let add f1 f2 = f1 +. f2

  let mul f1 f2 = f1 *. f2

  let equal f1 f2 = (f1 = f2)

  let leq f1 f2 = f1 <= f2

  let lt f1 f2 = f1 < f2

  let geq f1 f2 = f1 >= f2
  
  let gt f1 f2 = f1 > f2

  let compare = Pervasives.compare

  let max = Pervasives.max

  let to_string f = Printf.sprintf "%g" f

  let format fmt f = Format.pp_print_string fmt (to_string f)

end
