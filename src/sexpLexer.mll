{
  open Lexing
  open SexpParser

  let get_id name =
    String.sub name 1 (String.length name - 1)
}

let whitespace = ['\t' ' ']+
let newline = "\r\n" | '\n' | '\r'
let digit = ['0'-'9']
let pos_int_const = digit+
let float_const =
  '-'? ('.' digit+ | digit+ ('.' digit*)?) (['e' 'E'] ['+' '-']? digit+)?
let var = 'x' digit+
let param = 'p' digit+

(* TODO: Comments. *)
rule read =
  parse
  | whitespace { read lexbuf }
  | newline { read lexbuf }
  | "true" { TRUE }
  | "false" { FALSE }
  | "F" { F }
  | "G" { G }
  | "U" { U }
  | "R" { R }
  | "and" { AND }
  | "or" { OR }
  | "not" { NOT }
  | "<=" { LEQ }
  | ">=" { GEQ }
  | "(" { LP }
  | ")" { RP }
  | "inf" { INF }
  | pos_int_const { POS_INT_CONST (int_of_string (lexeme lexbuf)) }
  | float_const { FLOAT_CONST (float_of_string (lexeme lexbuf)) }
  | var { VAR (int_of_string (get_id (lexeme lexbuf))) }
  | param { PARAM (int_of_string (get_id (lexeme lexbuf))) }
  | eof { EOF }
