open U
open Format
open FormatU

module DynArray = Batteries.DynArray
module Array = Batteries.Array
module Option = Batteries.Option

type 'a segment_t = {
  segment_end: int;
  segment_data: 'a
}

let mk_segment end_t data =
  check_arg (end_t >= 0) "Segment end time should be >= 0.";
  { segment_end = end_t; segment_data = data }

let segment_end seg =
  seg.segment_end

let segment_data seg =
  seg.segment_data

let segment_equal d_equal seg1 seg2 =
  (segment_end seg1 = segment_end seg2) &&
  (d_equal (segment_data seg1) (segment_data seg2))

type 'a t = {
  t_segments: 'a segment_t DynArray.t
} [@@unboxed]

let seg_len series =
  DynArray.length series.t_segments

let get_seg series i =
  DynArray.get series.t_segments i

let try_get_seg series i =
  DynArrayU.try_get series.t_segments i

let time_len series =
  let s_len = seg_len series in
  if s_len = 0 then
    0
  else
    segment_end (get_seg series (s_len - 1)) + 1

let end_time series =
  (time_len series) - 1

let at_t series t =
  check_arg (t >= 0) "Time point should be >= 0.";
  let rec at_rec i =
    let seg = get_seg series i in
    if (segment_end seg) >= t then
      segment_data seg
    else
      at_rec (i + 1)
  in
  try
    at_rec 0
  with
  | Invalid_argument _ -> invalid_arg "Time point should be below the duration."

let equal d_equal series1 series2 =
  DynArrayU.equal
    ~elem_equal:(segment_equal d_equal)
    series1.t_segments
    series2.t_segments

let mk_empty () =
  { t_segments = DynArray.create () }

let of_segment segment =
  { t_segments = (DynArray.init 1 (fun _ -> segment)) }

let mk_const end_t data =
  of_segment (mk_segment end_t data)

let segment_array_add d_equal segments new_segment =
  if not (DynArray.empty segments) then
    let last = DynArray.last segments in
    check_arg
      ((segment_end new_segment) > (segment_end last))
      "New segment should end after the last existing one.";
    if d_equal (segment_data new_segment) (segment_data last) then
      DynArray.delete_last segments
    else ()
  else ();
  DynArray.add segments new_segment

let of_segment_array d_equal arr =
  let segments = DynArray.make (Array.length arr) in
  Array.iter (segment_array_add d_equal segments) arr;
  { t_segments = segments }

let of_segment_dynarray d_equal arr =
  let segments = DynArray.make (DynArray.length arr) in
  DynArray.iter (segment_array_add d_equal segments) arr;
  { t_segments = segments }

let of_sample_array d_equal samples =
  let len = Array.length samples in
  if len = 0 then
    mk_empty ()
  else begin
    let segments = DynArray.make len in
    let rec add_rec last_end last_data i =
      if i >= len then
        DynArray.add segments (mk_segment last_end last_data)
      else
        let data = Array.get samples i in
        if not (d_equal data last_data) then
          DynArray.add segments (mk_segment last_end last_data)
        else ();
        add_rec (last_end + 1) data (i + 1)
    in
    add_rec 0 (Array.get samples 0) 1;
    { t_segments = segments }
  end

let of_sample_dynarray d_equal samples =
  (* TODO: Now this is a copy+paste of of_sample_array. Maybe try to extract some
     commen code in a separate function. *)
  let len = DynArray.length samples in
  if len = 0 then
    mk_empty ()
  else begin
    let segments = DynArray.make len in
    let rec add_rec last_end last_data i =
      if i >= len then
        DynArray.add segments (mk_segment last_end last_data)
      else
        let data = DynArray.get samples i in
        if not (d_equal data last_data) then
          DynArray.add segments (mk_segment last_end last_data)
        else ();
        add_rec (last_end + 1) data (i + 1)
    in
    add_rec 0 (DynArray.get samples 0) 1;
    { t_segments = segments }
  end

let to_sample_array series =
  let samples = DynArray.make (time_len series) in
  let s_len = seg_len series in
  let rec to_array_rec prev_end i =
    if i < s_len then begin
      let { segment_end; segment_data } = get_seg series i in
      for j = 1 to (segment_end - prev_end) do
        DynArray.add samples segment_data
      done;
      to_array_rec segment_end (i + 1)
    end else ()
  in
  to_array_rec (-1) 0;
  DynArray.to_array samples

let map2 d_equal fn1 fn2 series =
  let s_len = seg_len series in
  if s_len = 0 then
    mk_empty ()
  else
    let segments = DynArray.make (seg_len series) in
    let rec map2_rec last_end last_data i =
      if i >= s_len then
        DynArray.add segments (mk_segment last_end (fn2 last_data))
      else
        let seg = get_seg series i in
        let data = fn1 (segment_data seg) in
        if not (d_equal data last_data) then
          DynArray.add segments (mk_segment last_end (fn2 last_data))
        else ();
        map2_rec (segment_end seg) data (i + 1)
    in
    let seg0 = get_seg series 0 in
    map2_rec (segment_end seg0) (fn1 (segment_data seg0)) 1;
    { t_segments = segments }

let map_distinct fn series =
  let segments = DynArray.map
    (fun s -> mk_segment (segment_end s) (fn (segment_data s)))
    series.t_segments
  in
  { t_segments = segments }

let join_array d_equal d_join arg_series =
  (* NOTE: Series should have the same duration,
     otherwise the result will be truncated. *)
  (* TODO: Maybe allow series of different duration. *)
  let args_n = Array.length arg_series in
  check_arg (args_n > 0) "Array of arguments should not be empty.";
  let args_end_t = end_time arg_series.(0) in
  for i = 0 to args_n - 1 do
    check_arg
      (end_time arg_series.(i) = args_end_t)
      "Arguments should have the same duration."
  done;
  if args_end_t  = (-1) then
    mk_empty ()
  else begin
    let max_arg_len = Array.fold_left
      (fun acc series -> max acc (seg_len series)) 0 arg_series
    in
    let cur_segments = Array.make args_n 0 in
    let result = DynArray.make max_arg_len in
    let rec join_array_rec () =
      let rec find_min_end min_end i =
        if i >= args_n then
          min_end
        else
          let series = arg_series.(i) in
          let cur_seg = cur_segments.(i) in
          find_min_end (min min_end (segment_end (get_seg series cur_seg))) (i + 1)
      in
      let min_end = find_min_end max_int 0 in
      let rec join_advance_rec data i =
        if i >= args_n then
          data
        else begin
          let series = arg_series.(i) in
          let cur_i = cur_segments.(i) in
          let cur_seg = get_seg series cur_i in
          if segment_end cur_seg = min_end then
            cur_segments.(i) <- cur_i + 1
          else ();
          join_advance_rec (d_join data (segment_data cur_seg)) (i + 1)
        end        
      in
      let series0 = arg_series.(0) in
      let cur_i0 = cur_segments.(0) in
      let cur_seg0 = get_seg series0 cur_i0 in
      if segment_end cur_seg0 = min_end then
        cur_segments.(0) <- cur_i0 + 1
      else ();
      let joined_data = join_advance_rec (segment_data cur_seg0) 1 in
      segment_array_add d_equal result (mk_segment min_end joined_data);
      if min_end < args_end_t then
        join_array_rec ()
      else ()
    in
      join_array_rec ();
      { t_segments = result }
  end

let format_segment format_data fmt seg =
  pp_open_box fmt 0;
  pp_print_string fmt "to";
  pp_print_space fmt ();
  pp_print_int fmt (segment_end seg);
  pp_print_space fmt ();
  pp_print_string fmt "->";
  pp_print_space fmt ();
  format_data fmt (segment_data seg);
  pp_close_box fmt ()

let format
  ?(print_sep=(fun fmt -> pp_print_string fmt ";"; pp_print_space fmt ()))
  format_data fmt series =
    pp_open_box fmt 0;
    format_dynarray
      ~print_sep:print_sep
      (format_segment format_data)
      fmt
      series.t_segments;
    pp_close_box fmt ()

let to_string format_data series =
  asprintf "%a" (format format_data) series

(** Adds a segment to a backshift result. *)
let backshift_add d_join_cmp d_equal segments add_start_t add_end_t add_data =
  let add_start_t = max 0 add_start_t in
  let orig_len = DynArray.length segments in
  (* Removes the empty space in the segment array (if any). Read_i + 1 is the
     first unoccupied place, and write_i is the last unoccupied. *)
  let add_finish read_i write_i =
    if write_i > read_i then
      DynArray.delete_range segments (read_i + 1) (write_i - read_i)
    else ();
  in
  let rec add_rec read_i seg_i write_i prev_data =
    let seg_i_data = segment_data seg_i in
    let (cmp, new_data) = d_join_cmp prev_data seg_i_data in
    if new_data == prev_data then begin
      add_move_next seg_i (read_i - 1) write_i prev_data (cmp_is_eq cmp)
    end else if new_data == seg_i_data then
      add_finish read_i write_i
    else begin (* if incomparable *)
      DynArray.set segments write_i (mk_segment (segment_end seg_i) new_data);
      (* NOTE: We intentionally propagate 'new_data' to the next
         (previous in time) segment. If the next segment is affected by the
         added interval, its data is "strictly greater" than data of the current
         one, thus joining 'new_data' and 'add_data' to the data of the next
         segment produces the same results. But when we propagate 'new_data',
         we can also correctly merge segments with equal data. *)
      add_move_next seg_i (read_i - 1) (write_i - 1) new_data false
    end
  and add_move_next seg_i next_read_i next_write_i new_data new_eq_i =
    if next_read_i < 0 then begin
      (* TODO: Use the result of d_join_cmp instead of calling d_equal. *)
      if (add_start_t = 0) || new_eq_i then
        add_finish next_read_i next_write_i
      else begin (* if add_start_t > 0 and the split segment has different data. *)
        DynArray.set segments next_write_i
          (mk_segment (add_start_t - 1) (segment_data seg_i));
        add_finish next_read_i (next_write_i - 1)
      end
    end else begin
      let next_seg_i = DynArray.get segments next_read_i in
      let start_t = (segment_end next_seg_i) + 1 in
      if add_start_t < start_t then
        add_rec next_read_i next_seg_i next_write_i new_data
      else if add_start_t = start_t then begin
        if d_equal (segment_data next_seg_i) new_data then
          (* If the start of the backshifted segment coinsides with the start of
             some existing segment, and its data is the same as the data in
             the previous segment, the previous segment should be removed. *)
          add_finish (next_read_i - 1) next_write_i
        else
          add_finish next_read_i next_write_i
      end else begin (* if add_start_t > start_t *)
        (* TODO: We d_join_cmp can produce this result for us. *)
        if new_eq_i then
          add_finish next_read_i next_write_i
        else begin
          DynArray.set segments next_write_i
            (mk_segment (add_start_t - 1) (segment_data seg_i));
          add_finish next_read_i (next_write_i - 1)
        end
      end
    end
  in
  if add_end_t < 0 then
    ()
  else if orig_len = 0 then
    DynArray.add segments (mk_segment add_end_t add_data)
  else begin
    let tail_seg = mk_segment add_end_t add_data in
    (* NOTE: If add_start_t > 0, backshift_add increases the length by at most
       two. If add_start_t = 0, it increases the length by at most one.
       Here, we are making room for the potential new elements. *)
    DynArray.add segments tail_seg;
    if add_start_t > 0 then
      DynArray.add segments tail_seg
    else ();
    add_rec
      (orig_len - 1)
      (DynArray.get segments (orig_len - 1))
      (DynArray.length segments - 2)
      add_data
  end

(* NOTE: d_join_cmp should have the same propertues as URects.union_cmp. *)
let backshift d_join_cmp d_equal padding min_shift max_shift series =
  check_arg (min_shift >= 0) "Lower shift bound should be at least zero.";
  check_arg (max_shift >= min_shift)
    "Upper shift bound should not be less than lower bound.";
  if max_shift = 0 then (* Means that min_shift is also 0. *)
    series
  else begin
    let s_len = seg_len series in
    let t_len = time_len series in
    let result = DynArray.make s_len in
    (* Repeatedly backshifts segments and joins them to the segment array. *)
    let rec backshift_forward_rec prev_end i =
      if i >= s_len then
        ()
      else begin
        let seg = get_seg series i in
        let seg_end = segment_end seg in
        (* NOTE: max_shift can be max_int to denote +inf. If prev_end is in
           [0, max_int-1], the result of subtracting max_int will be <= 0, which
           is fine. *)
        let shifted_start = prev_end + 1 - max_shift in
        let shifted_end = seg_end - min_shift in
        backshift_add
          d_join_cmp d_equal result shifted_start shifted_end (segment_data seg);
        backshift_forward_rec seg_end (i + 1)
      end
    in
    let rec backshift_backward_rec acc i =
      if i < 0 then
        ()
      else begin
        let { segment_end; segment_data } = get_seg series i in
        let shifted_end = segment_end - min_shift in
        if shifted_end < 0 then
          ()
        else begin
          let cmp, new_acc = d_join_cmp acc segment_data in
          if new_acc != acc then
            DynArray.add result (mk_segment shifted_end new_acc)
          else ();
          backshift_backward_rec new_acc (i - 1)
        end
      end
    in
    let backshift_backward i =
      if i < 0 then
        ()
      else
        let { segment_end; segment_data } = get_seg series i in
        let shifted_end = segment_end - min_shift in
        if shifted_end < 0 then
          ()
        else begin
          DynArray.add result (mk_segment shifted_end segment_data);
          backshift_backward_rec segment_data (i - 1);
          DynArrayU.rev_inplace result
        end
    in
    let rec find_first_rec prev_end i =
      if i >= s_len then
        backshift_backward (i - 1)
      else
        let shifted_start = prev_end + 1 - max_shift in
        if shifted_start > 0 then begin
          backshift_backward (i - 1);
          backshift_forward_rec prev_end i
        end else
          find_first_rec (segment_end (get_seg series i)) (i + 1)
    in
    find_first_rec (-1) 0;
    if min_shift > 0 then
      segment_array_add d_equal result (mk_segment (t_len - 1) padding)
    else ();
    { t_segments = result }
  end

(* TODO: Don't use d_neutral. *)
let until d_union d_intersect d_neutral d_equal series1 series2 =
  let t_len = time_len series1 in
  check_arg
    (t_len = time_len series2)
    "Arguments should have the same duration.";
  let s_len1 = seg_len series1 in
  let s_len2 = seg_len series2 in
  let seg_i_end series i =
    if i = (-1) then
      -1
    else
      segment_end (get_seg series i)
  in
  let rev_result = DynArray.make (max s_len1 s_len2) in
  let rev_result_add seg =
    if (DynArray.length rev_result = 0) ||
       (not (d_equal (segment_data (DynArray.last rev_result)) (segment_data seg)))
    then
      DynArray.add rev_result seg
    else ()
  in
  let rec until_rec next_i1 next_end1 next_i2 next_end2 prev_end val1 val2 acc =
    rev_result_add (mk_segment prev_end (d_union acc val2));
    if next_end1 >= 0 || next_end2 >= 0 then begin
      if next_end2 > next_end1 then
        let next_data2 = segment_data (get_seg series2 next_i2) in
        until_rec
          next_i1 next_end1 (next_i2 - 1) (seg_i_end series2 (next_i2 - 1))
          next_end2 val1 next_data2 (d_union acc (d_intersect val1 val2))
      else if next_end1 > next_end2 then
        let next_data1 = segment_data (get_seg series1 next_i1) in
        until_rec
          (next_i1 - 1) (seg_i_end series1 (next_i1 - 1)) next_i2 next_end2
          next_end1 next_data1 val2 (d_intersect acc next_data1)
      else (* if next_end1 = next_end2 *)
        let next_data1 = segment_data (get_seg series1 next_i1) in
        let next_data2 = segment_data (get_seg series2 next_i2) in
        until_rec
          (next_i1 - 1) (seg_i_end series1 (next_i1 - 1))
          (next_i2 - 1) (seg_i_end series2 (next_i2 - 1))
          next_end1 next_data1 next_data2 (d_intersect (d_union acc val2) next_data1)
    end else ()
  in
  if t_len = 0 then
    mk_empty ()
  else begin
    until_rec
      (s_len1 - 2) (seg_i_end series1 (s_len1 - 2))
      (s_len2 - 2) (seg_i_end series2 (s_len2 - 2))
      (t_len - 1)
      (segment_data (get_seg series1 (s_len1 - 1)))
      (segment_data (get_seg series2 (s_len2 - 1)))
      d_neutral;
    DynArrayU.rev_inplace rev_result;
    { t_segments = rev_result }
  end

        
