
let with_in_file_chan ?stdin_name fn file_name =
  let with_in_file_chan_impl () =
    let chan = open_in file_name in
    try
      let result = fn chan in
      close_in chan;
      result
    with
    | e ->
        close_in chan;
        raise e
  in
  match stdin_name with
  | None -> with_in_file_chan_impl ()
  | Some name ->
      if name = file_name then
        fn stdin
      else
        with_in_file_chan_impl ()
