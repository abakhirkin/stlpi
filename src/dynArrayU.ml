open Batteries.DynArray

let for_all pred arr =
  let len = length arr in
  let rec for_all_rec i =
    if i >= len then
      true
    else
      (pred (get arr i)) && (for_all_rec (i + 1))
  in
  for_all_rec 0

let exists pred arr =
  let len = length arr in
  let rec exists_rec i =
    if i >= len then
      false
    else
      (pred (get arr i)) || (exists_rec (i + 1))
  in
  exists_rec 0

let set_ij arr i j =
  if i <> j then
    set arr i (get arr j)
  else ()

let swap arr i j =
  if i <> j then begin
    let arr_i = get arr i in
    set arr i (get arr j);
    set arr j arr_i;
  end else ()

let iter_cartesian fn arr1 arr2 =
  for i = 0 to (length arr1) - 1 do
    for j = 0 to (length arr2) - 1 do
      fn (get arr1 i) (get arr2 j)
    done
  done

let swap_remove arr i =
  let len = length arr in
  if i <> len - 1 then
    set arr i (get arr (len - 1))
  else ();
  delete_last arr

let equal ?(elem_equal=(=)) arr1 arr2 =
  let len = length arr1 in
  if length arr2 <> len then
    false
  else
    let rec equal_rec i =
      if i >= len then
        true
      else
        (elem_equal (get arr1 i) (get arr2 i)) &&
        (equal_rec (i + 1))
    in
    equal_rec 0

let try_get arr i =
  try
    Some (get arr i)
  with
  | Invalid_arg _ -> None

let rev_inplace arr =
  let rec rev_loop i j =
    if i >= j then
      ()
    else begin
      swap arr i j;
      rev_loop (i + 1) (j - 1)
    end
  in
  rev_loop 0 ((length arr) -  1)

