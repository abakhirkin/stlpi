open Array

module DynArray = Batteries.DynArray

let of_iter iter_fn len elems =
  let arr = DynArray.make len in
  iter_fn (DynArray.add arr) elems;
  DynArray.to_array arr

let fold_left2 fn init arr1 arr2 =
  let acc = ref init in
  iter2
    (fun elem1 elem2 -> acc := (fn !acc elem1 elem2))
    arr1
    arr2;
  !acc
