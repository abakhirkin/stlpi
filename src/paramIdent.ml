open U
open Format
open FormatU
open Series

module Make(N: Numbers.T) = struct

  module Pstl = Pstl.Make(N)
  open Pstl


  module Rects = URects.Make(N)

  
  module ParamHash = struct
    type t = param_t

    let equal p1 p2 = (param_id p1) = (param_id p2)

    let hash p = param_id p
  end

  module ParamHashtbl = Hashtbl.Make(ParamHash)
  module ParamHashtblU = HashtblU.Make(ParamHashtbl)

  type polarity_t = Pos | Neg

  type pdata_t = {
    pdata_polarity: polarity_t;
    pdata_dim: int
  }

  let pdata_polarity pd =
    pd.pdata_polarity

  let pdata_dim pd =
    pd.pdata_dim

  let mk_pdata polarity dim =
    { pdata_polarity = polarity; pdata_dim = dim }

  type pspace_t = {
    pspace_param_n: int;
    pspace_print_order: param_t array;
    pspace_data: pdata_t ParamHashtbl.t
  }
  
  let pspace_data ps param =
    ParamHashtbl.find ps.pspace_data param

  let pspace_rect_dim ps =
    (0, ps.pspace_param_n)

  let mk_pspace formula =
    let param_n = ref 0 in
    let param_data = ParamHashtbl.create HashtblU.default_size in    
    let add_param param polarity =
      try
        let data = ParamHashtbl.find param_data param in        
        check_arg
          ((pdata_polarity data) = polarity)
          (sprintf "Parameter p%d occurs both positive and negative." (param_id param))
      with
      | Not_found ->
          param_n := !param_n + 1;
          ParamHashtbl.replace param_data param (mk_pdata polarity (!param_n - 1))
    in
    let rec mk_pspace_rec formula =
      begin match formula with
      | Leq (_, SParam param) ->
          add_param param Pos
      | Leq _ ->
          ()
      | Geq (_, SParam param) ->
          add_param param Neg
      | Geq _ ->
          ()
      (* TODO: Some uniform handling of intervals to avoid copy/paste of cases. *)
      | F ({intvl_lower = TLit _; intvl_upper = TParam param}, arg_formula) ->
          invalid_arg "Time parameters are not supported."
      | F ({intvl_lower = TParam param; intvl_upper = TLit _}, arg_formula) ->
          invalid_arg "Time parameters are not supported."
      | F ({intvl_lower = TParam _; intvl_upper = TParam _}, arg_formula) ->
          invalid_arg "Only one bound of a time interval can be a parameter."
      | F (_, arg_formula) ->
          mk_pspace_rec arg_formula
      | G ({intvl_lower = TLit _; intvl_upper = TParam param}, arg_formula) ->
          invalid_arg "Time parameters are not supported."
      | G ({intvl_lower = TParam param; intvl_upper = TLit _}, arg_formula) ->
          invalid_arg "Time parameters are not supported."
      | G ({intvl_lower = TParam _; intvl_upper = TParam _}, arg_formula) ->
          invalid_arg "Only one bound of a time interval can be a parameter."
      | G (_, arg_formula) ->
          mk_pspace_rec arg_formula
      | U ({intvl_lower = TLit _; intvl_upper = TParam param}, arg1, arg2) ->
          invalid_arg "Time parameters are not supported."
      | U ({intvl_lower = TParam param; intvl_upper = TLit _}, arg1, arg2) ->
          invalid_arg "Time parameters are not supported."
      | U ({intvl_lower = TParam _; intvl_upper = TParam _}, arg1, arg2) ->
          invalid_arg "Only one bound of a time interval can be a parameter."
      | U (_, arg1, arg2) ->
          mk_pspace_rec arg1;
          mk_pspace_rec arg2
      | R ({intvl_lower = TLit _; intvl_upper = TParam param}, arg1, arg2) ->
          invalid_arg "Time parameters are not supported."
      | R ({intvl_lower = TParam param; intvl_upper = TLit _}, arg1, arg2) ->
          invalid_arg "Time parameters are not supported."
      | R ({intvl_lower = TParam _; intvl_upper = TParam _}, arg1, arg2) ->
          invalid_arg "Only one bound of a time interval can be a parameter."
      | R (_, arg1, arg2) ->
          mk_pspace_rec arg1;
          mk_pspace_rec arg2
      | True ->
          ()
      | False ->
          ()
      | And arg_formulas ->
          Array.iter mk_pspace_rec arg_formulas
      | Or arg_formulas ->
          Array.iter mk_pspace_rec arg_formulas
      | Not _ ->
          invalid_arg "Formula should be in NNF."
      end
    in
    mk_pspace_rec formula;
    let params = ArrayU.of_iter
      ParamHashtblU.iter_keys (ParamHashtbl.length param_data) param_data in
    Array.sort (fun p1 p2 -> (param_id p1) - (param_id p2)) params;
    { pspace_param_n = !param_n;
      pspace_print_order = params;
      pspace_data = param_data }

  let format_space_bound fmt (param, polarity, bound) =
    pp_open_box fmt 0;
    if polarity = Neg then
      pp_print_string fmt "-p"
    else
      pp_print_string fmt "p";
    pp_print_int fmt (param_id param);
    pp_print_space fmt ();
    pp_print_string fmt ">=";
    pp_print_space fmt ();
    N.format fmt bound;
    pp_close_box fmt ()

  let format_point pspace fmt pt =
    let iter_bounded_params fn params =
      Array.iter
        (fun param ->
          let data = pspace_data pspace param in
          let bound = Rects.point_get_n pt (pdata_dim data) in
          if N.gt bound N.minus_inf then
            fn (param, (pdata_polarity data), bound)
          else ())
        params
    in
    pp_open_box fmt 0;
    pp_print_string fmt "(";
    format_iter
      ~print_sep:(fun fmt -> pp_print_string fmt ","; pp_print_space fmt ())
      format_space_bound iter_bounded_params fmt pspace.pspace_print_order;
    pp_print_string fmt ")";
    pp_close_box fmt ()

  let format_rects pspace fmt rects =
    check_arg
      (pspace_rect_dim pspace = Rects.dim rects)
      "Parameter space and the set of rectangles should have the same dimension.";
    pp_open_box fmt 0;
    pp_print_string fmt "[";
    if Rects.is_empty rects then
      pp_print_string fmt "false"
    else if Rects.is_top rects then
      pp_print_string fmt "true"
    else
      format_iter
        ~print_sep:(fun fmt -> pp_print_string fmt ";"; pp_print_space fmt ())
        (format_point pspace) Rects.iter fmt rects;
    pp_print_string fmt "]";
    pp_close_box fmt ()

  let rects_to_string pspace rects =
    asprintf "%a" (format_rects pspace) rects

  let format_series ?print_sep pspace fmt series =
    Series.format ?print_sep:print_sep (format_rects pspace) fmt series

  let series_to_string pspace series =
    asprintf "%a" (format_series pspace) series
  
  let identify formula signal =
    let pspace = mk_pspace formula in
    let t_len = Signal.time_len signal in
    (* NOTE: If t_len = 0, we don't need the below, but we trade optimized handling
       of empty signals for 1 less indentation level. *)
    let rect_dim = pspace_rect_dim pspace in
    let top = Rects.mk_top rect_dim in
    let empty = Rects.mk_empty rect_dim in
    let rec identify_rec formula =
      match formula with
      | True ->
          Series.mk_const (t_len - 1) top
      | False ->
          Series.mk_const (t_len - 1) empty
      | Leq (x, SLit c) ->
          Series.map2
            (=)
            (fun v -> v <= c)
            (fun b -> if b then top else empty)
            (Signal.get signal (var_id x))
      | Geq (x, SLit c) ->
          Series.map2
            (=)
            (fun v -> v >= c)
            (fun b -> if b then top else empty)
            (Signal.get signal (var_id x))
      | Leq (x, SParam p) ->
          let data = pspace_data pspace p in
          let p_dim = pdata_dim data in
          Series.map_distinct
            (fun v -> Rects.of_n_bound rect_dim p_dim v)
            (Signal.get signal (var_id x))
      | Geq (x, SParam p) ->
          let data = pspace_data pspace p in
          let p_dim = pdata_dim data in
          Series.map_distinct
            (fun v -> Rects.of_n_bound rect_dim p_dim (N.neg v))
            (Signal.get signal (var_id x))
      | F ({intvl_lower = TLit lower; intvl_upper = TLit upper}, arg_formula) ->
          let arg_series = identify_rec arg_formula in
          (* NOTE: We don't check interval bounds here, assuming they were checked
             by the interval constructor. *)
          Series.backshift Rects.union_cmp Rects.equal empty lower upper arg_series
      | F _ ->
          invalid_arg "Time parameters are not supported."
      | G ({intvl_lower = TLit lower; intvl_upper = TLit upper}, arg_formula) ->
          (* NOTE: We don't check interval bounds here, assuming they were checked
             by the interval constructor. *)
          let series =
            Series.map_distinct Rects.negate_flip (identify_rec arg_formula)
          in
            Series.map_distinct
              Rects.negate_flip
              (Series.backshift Rects.union_cmp Rects.equal empty lower upper series)
      | G _ ->
          invalid_arg "Time parameters are not supported."
      | U ({intvl_lower = TLit lower; intvl_upper = TLit upper}, arg1, arg2) ->
          check_arg
            (lower = 0 && upper = max_int)
            "Timed 'until' (other than [0, inf)) is not supported.";
          let series1 = identify_rec arg1 in
          let series2 = identify_rec arg2 in
          (* TODO: try with/without double negation and compare performance. *)
          Series.until Rects.union Rects.intersect empty Rects.equal series1 series2
      | U _ ->
          invalid_arg "Time parameters are not supported."
      | R ({intvl_lower = TLit lower; intvl_upper = TLit upper}, arg1, arg2) ->
          check_arg
            (lower = 0 && upper = max_int)
            "Timed 'R' (other than [0, inf)) is not supported.";
          let series1 = identify_rec arg1 in
          let series2 = identify_rec arg2 in
          Series.until Rects.intersect Rects.union top Rects.equal series1 series2
      | R _ ->
          invalid_arg "Time parameters are not supported."
      | And arg_formulas ->
          let arg_series = Array.map identify_rec arg_formulas in
          Series.join_array Rects.equal Rects.intersect arg_series
      | Or arg_formulas ->
          let arg_series = Array.map identify_rec arg_formulas in
          Series.join_array Rects.equal Rects.union arg_series
      | Not _ -> invalid_arg "Formula should be in NNF."
    in
    if t_len = 0 then
      (pspace, Series.mk_empty ())
    else
      (pspace, identify_rec formula)

end

