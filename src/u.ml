
let check_arg cond msg =
  if not cond then
    invalid_arg msg
  else ()

  type cmp_t = {
    cmp_flags: int
  } [@@unboxed]

  let cmp_incomp =
    { cmp_flags = 0 }

  let cmp_lt =
    { cmp_flags = 1 }

  let cmp_gt =
    { cmp_flags = 2 }

  let cmp_eq =
    { cmp_flags = 3 }

  let cmp_of_compare i =
    { cmp_flags =
        if i < 0 then
          1
        else if i > 0 then
          2
        else (* if i = 0 *)
          3 }

  let cmp_is_incomp cmp =
    cmp.cmp_flags = 0

  let cmp_is_leq cmp =
    (cmp.cmp_flags land 1) <> 0

  let cmp_is_lt cmp =
    cmp.cmp_flags = 1

  let cmp_is_geq cmp =
    (cmp.cmp_flags land 2) <> 0

  let cmp_is_gt cmp =
    cmp.cmp_flags = 2

  let cmp_is_eq cmp =
    cmp.cmp_flags = 3

  let cmp_meet cmp1 cmp2 =
    { cmp_flags = cmp1.cmp_flags land cmp2.cmp_flags }

  let string_of_cmp cmp =
    match cmp.cmp_flags with
    | 0 -> "<>"
    | 1 -> "<"
    | 2 -> ">"
    | 3 -> "="
    | _ -> invalid_arg "Flags should be between 0 and 3."
