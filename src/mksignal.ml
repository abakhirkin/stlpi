open Format
open FormatU

let pi = 4.0 *. atan 1.0

let mk_sin_samples ?(ampl = 1.0) ?(phase = 0) period len =
  let fperiod = float_of_int period in
  for i = 0 to len - 1 do
    let sample =
      (ampl *. (sin (2.0 *. pi *. (float_of_int (i + phase)) /. fperiod)))
    in
    printf "%a@." Numbers.Float.format sample
  done

let mk_1ex_sin_samples ?(ampl = 1.0) ?(phase = 0) period len =
  let fperiod = float_of_int period in
  for i = 0 to len - 1 do
    let sinarg = 2.0 *. pi *. (float_of_int (i + phase)) /. fperiod in
    let exparg = 2.0 *. pi *. (float_of_int i) /. fperiod in
    let sample = ampl *. (sin sinarg) *. (1. +. (exp (-. exparg))) in
    printf "%a@." Numbers.Float.format sample
  done

let mk_sin () =
  let period = (int_of_string Sys.argv.(2)) in
  let phase = (int_of_string Sys.argv.(3)) in
  let len = (int_of_string Sys.argv.(4)) in
  mk_sin_samples ~phase:phase period len

let mk_1ex_sin () =
  let period = (int_of_string Sys.argv.(2)) in
  let phase = (int_of_string Sys.argv.(3)) in
  let len = (int_of_string Sys.argv.(4)) in
  mk_1ex_sin_samples ~phase:phase period len

let mk_square () =
  let stable_from = 250 in
  let stable_to = 750 in
  let edge_from = 25 in
  let edge_to = 50 in
  let hi_from = 6. in
  let hi_to = 8. in
  let lo_from = 0. in
  let lo_to = 2. in
  let len = 1000000 in
  let mk_sample v =
    printf "%a@." Numbers.Float.format v
  in
  let rec lo_loop lo_len len =
    if len = 0 then
      ()
    else if lo_len = 0 then
      rise_loop lo_to (RandomU.int_in edge_from edge_to) len
    else begin
      let v = RandomU.float_in lo_from lo_to in
      mk_sample v;
      lo_loop (lo_len - 1) (len - 1)
    end
  and rise_loop prev_v raise_len len =
    if len = 0 then
      ()
    else if raise_len = 0 then
      hi_loop (RandomU.int_in stable_from stable_to) len
    else begin
      let v = RandomU.float_in prev_v hi_from in
      mk_sample v;
      rise_loop v (raise_len - 1) (len - 1)
    end
  and hi_loop hi_len len =
    if len = 0 then
      ()
    else if hi_len = 0 then
      fall_loop hi_from (RandomU.int_in edge_from edge_to) len
    else begin
      let v = RandomU.float_in hi_from hi_to in
      mk_sample v;
      hi_loop (hi_len - 1) (len - 1)
    end
  and fall_loop prev_v fall_len len =
    if len = 0 then
      ()
    else if fall_len = 0 then
      lo_loop (RandomU.int_in stable_from stable_to) len
    else begin
      let v = RandomU.float_in lo_to prev_v in
      mk_sample v;
      fall_loop v (fall_len - 1) (len - 1)
    end
  in
  lo_loop (RandomU.int_in stable_from stable_to) len
  

let () =
  Random.self_init ();
  match Sys.argv.(1) with
  | "sin" -> mk_sin ()
  | "square" -> mk_square ()
  | _ -> invalid_arg (sprintf "Unknown signal type: %s." Sys.argv.(1))
