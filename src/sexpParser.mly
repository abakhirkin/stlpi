%{
  module Fpstl = Pstl.Fpstl
%}


%token TRUE
%token FALSE
%token <int> VAR
%token <int> PARAM
%token <int> POS_INT_CONST
%token <float> FLOAT_CONST
%token INF
%token LEQ
%token GEQ
%token F
%token G
%token U
%token R
%token AND
%token OR
%token NOT
%token LP
%token RP
%token EOF

(* NOTE: The type 'Pstl.Fpstl.formula_t' will appear in the .mli file literally,
   regardless of any module declarations above. *)
%start <Pstl.Fpstl.formula_t> top_formula

%%

top_formula: f = formula; EOF { f }

formula:
  | TRUE
    { Fpstl.True }
  | FALSE
    { Fpstl.False }
  | LP; LEQ; v_id = VAR; c = space_const; RP
    { Fpstl.Leq (Fpstl.mk_var v_id, c) }
  | LP; GEQ; v_id = VAR; c = space_const; RP
    { Fpstl.Geq (Fpstl.mk_var v_id, c) }
  | LP; F; intvl = time_intvl; arg = formula; RP
    { Fpstl.mk_f intvl arg }
  | LP; G; intvl = time_intvl; arg = formula; RP
    { Fpstl.mk_g intvl arg } 
  | LP; U; intvl = time_intvl; arg1 = formula; arg2 = formula; RP
    { Fpstl.mk_until intvl arg1 arg2 }
  | LP; R; intvl = time_intvl; arg1 = formula; arg2 = formula RP
    { Fpstl.mk_r intvl arg1 arg2 }
  | LP; AND; args = nonempty_list(formula); RP
    { Fpstl.mk_and (Array.of_list args) }
  | LP; OR; args = nonempty_list(formula); RP
    { Fpstl.mk_or (Array.of_list args) }
  | LP; NOT; arg = formula; RP
    { Fpstl.mk_not arg }

time_intvl:
  | LP; lb = time_const; ub = time_const; RP
    { Fpstl.mk_intvl lb ub }

time_const:
  | i = POS_INT_CONST { Fpstl.TLit i }
  | INF { Fpstl.TLit max_int }
  | p_id = PARAM { Fpstl.TParam (Fpstl.mk_param p_id) }

space_const:
  | f = float_const { Fpstl.SLit f }
  | p_id = PARAM { Fpstl.SParam (Fpstl.mk_param p_id) }

float_const:
  | i = POS_INT_CONST { float_of_int i }
  | f = FLOAT_CONST { f }
