open Format
module DynArray = Batteries.DynArray

let format_iter
  ?(print_sep=(fun fmt -> pp_print_space fmt ())) format_elem iter_fn fmt elems =
  pp_open_box fmt 0;
  let i = ref 0 in
  iter_fn
    (fun elem ->
      if !i <> 0 then
        print_sep fmt
      else ();
      format_elem fmt elem;
      i := !i + 1)
    elems;
  pp_close_box fmt ()

let format_array
  ?(print_sep=(fun fmt -> pp_print_space fmt ())) format_elem fmt elems =
  format_iter ~print_sep:print_sep format_elem Array.iter fmt elems

let format_array_nl format_elem fmt elems =
  format_array ~print_sep:(fun fmt -> pp_print_newline fmt ()) format_elem fmt elems

let format_dynarray
  ?(print_sep=(fun fmt -> pp_print_space fmt ())) format_elem fmt elems =
  format_iter ~print_sep:print_sep format_elem DynArray.iter fmt elems

let format_dynarray_nl format_elem fmt elems =
  format_dynarray ~print_sep:(fun fmt -> pp_print_newline fmt ()) format_elem fmt elems

let format_list
  ?(print_sep=(fun fmt -> pp_print_space fmt ())) format_elem fmt elems =
  format_iter ~print_sep:print_sep format_elem List.iter fmt elems
