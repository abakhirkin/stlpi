
let parse lexbuf =
  SexpParser.top_formula SexpLexer.read lexbuf

let parse_string s =
  parse (Lexing.from_string s)

let parse_chan chan =
  parse (Lexing.from_channel chan)

let parse_file ?stdin_name file_name =
  FileU.with_in_file_chan ?stdin_name:stdin_name parse_chan file_name

