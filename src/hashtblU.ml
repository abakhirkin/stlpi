let default_size = 16

module Make(HT: Hashtbl.S) = struct
  
  let iter_keys fn ht =
    HT.iter
      (fun k v -> fn k)
      ht

end
