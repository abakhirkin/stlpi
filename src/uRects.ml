(** Upward-closed rectangular set represented as a set of minimal points. *)

open U
open Format
open FormatU

module DynArray = Batteries.DynArray
module Array = Batteries.Array

(** Creates the module, where every coordinate of a point is either [int]
    or [N.t]. *)
module Make(N: Numbers.T) = struct

  (** Type of a point.

      A point has two vectors of coordinates, of type [int] and of type [N.t]. *)
  type point_t = {
    point_ints: int array;
    (** Array of [int] coordinates. *)
    point_ns: N.t array
    (** Array of [N.t] coordinates. *)
  }

  (** Dimension of a point.

      It is a tuple [(i, n)] where [i] is the number of integer coordinates
      and [n] is the number of [N.t] coordinates. *)
  let point_dim pt =
    (Array.length pt.point_ints, Array.length pt.point_ns)

  (** Gets the i-th [int] coordinate. *)
  let point_get_int pt i =
    Array.get pt.point_ints i

  (** Gets the i-th [N.t] coordinate. *)
  let point_get_n pt i =
    Array.get pt.point_ns i

  (** Creates a new point.
   
      [mk_point ints ns] creates a new point where [ints] is the array of integer
      coordinates and [ns] is the array of [N.t] coordinates.
      Arrays are not copied; the created point stores references to [ints] and
      [ns]. *)
  let mk_point ints ns =
    { point_ints = ints; point_ns = ns }

  (** Creates the greatest point.

      Every [int] coordinate of the greates point is [min_int].
      Every [N.t] coordinate of the greatest point is [N.minus_inf]. *)
  let mk_top_point (int_dim, n_dim) =
    { point_ints = Array.make int_dim min_int;
      point_ns = Array.make n_dim N.minus_inf }

  (** Whether the point is the greatest one. *)
  let point_is_top pt =
    (Array.for_all (fun v -> v = min_int) pt.point_ints) &&
    (Array.for_all (fun v -> N.equal v N.minus_inf) pt.point_ns)

  let point_is_bot pt =
    (Array.exists (fun v -> v = max_int) pt.point_ints) ||
    (Array.exists (fun v -> N.equal v N.inf) pt.point_ns)

  (** Partial order on points.
   
      [point_leq pt1 pt2] if every coordinate of [pt1] is {e greater-or-equal}
      than the corresponding coordinate of [pt2].
      This corresponds to inclusion order on the upward closures of the points. *)
  let point_leq pt1 pt2 =
    let (int_dim, n_dim) = point_dim pt1 in    
    let rec leq_ns_rec i =
      if i >= n_dim then
        true
      else
        (N.leq (point_get_n pt2 i) (point_get_n pt1 i)) && leq_ns_rec (i + 1)
    in
    let rec leq_ints_rec i =
      if i >= int_dim then
        leq_ns_rec 0
      else
        ((point_get_int pt1 i) >= (point_get_int pt2 i)) && leq_ints_rec (i + 1)
    in
    leq_ints_rec 0

  (** Compares two points according to the inclusion order of their upward
      closures.
      
      [point_compare pt1 pt2] is:
        - [cmp_eq] if the points are equal;
        - [cmp_lt] if the points are distinct and every coordinate of [pt1] is
          {e greater-or-equal} than the corresponding coordinate of [pt2];
        - [cmp_gt] if the points are distinct and every coordinate of [pt1] is
          {e less-or-equal} than the corresponding coordinate of [pt2];
        - [cmp_incomp] otherwise (i.e., if the points are incomparable). *)
  let point_compare pt1 pt2 =
    let (ints_dim, ns_dim) = point_dim pt1 in
    let rec compare_ns_rec cmp i =
      if i >= ns_dim then
        cmp
      else
        let cmp = cmp_meet cmp
          (cmp_of_compare (N.compare (point_get_n pt2 i) (point_get_n pt1 i))) in
        if cmp_is_incomp cmp then
          cmp
        else
          compare_ns_rec cmp (i + 1)
    in
    let rec compare_ints_rec cmp i =
      if i >= ints_dim then
        cmp
      else
        let cmp = cmp_meet cmp
          (cmp_of_compare (compare (point_get_int pt2 i) (point_get_int pt1 i))) in
        if cmp_is_incomp cmp then
          cmp
        else
          compare_ints_rec cmp (i + 1)
    in
    let cmp = compare_ints_rec cmp_eq 0 in
    if cmp_is_incomp cmp then
      cmp
    else
      compare_ns_rec cmp 0

  let point_intersect pt1 pt2 =
    let (ints_len, ns_len) = point_dim pt1 in
    check_arg
      ((ints_len, ns_len) = point_dim pt2) "Points should have the same dimension.";
    let ints = Array.init
      ints_len
      (fun i -> max (point_get_int pt1 i) (point_get_int pt2 i))
    in
    let ns = Array.init
      ns_len
      (fun i -> N.max (point_get_n pt1 i) (point_get_n pt2 i))
    in
    mk_point ints ns
    
  let point_of_int_bound point_dim dim bound =
    let pt = mk_top_point point_dim in
    Array.set pt.point_ints dim bound;
    pt

  let point_of_n_bound point_dim dim bound =
    let pt = mk_top_point point_dim in
    Array.set pt.point_ns dim bound;
    pt

  (** Formats a point. *)
  let format_point fmt pt =
    pp_open_box fmt 0;
    pp_print_string fmt "(";
    format_array pp_print_int fmt pt.point_ints;
    pp_print_string fmt ";";
    pp_print_space fmt ();
    format_array N.format fmt pt.point_ns;
    pp_print_string fmt ")";
    pp_close_box fmt ()

  (** Type of an upward-closed rectangular set,
      represented as a set of minimal points. *)
  type t = {
    t_dim: int * int;
    (** Dimension of the set (i.e., the dimension of every point). *)
    t_points: point_t DynArray.t
    (** Set of minimal points. *)
  }

  (** Gets the dimension of a set. *)
  let dim rects =
    rects.t_dim

  let length rects =
    DynArray.length rects.t_points

  let get rects i =
    DynArray.get rects.t_points i

  (** Creates an empty set of a given dimension. *)
  let mk_empty dim =
    { t_dim = dim; t_points = DynArray.create () }

  (** Whether the set is empty. *)
  let is_empty rects =
    DynArray.empty rects.t_points
  
  (** Whether the set is the greatest one. *)
  let is_top rects =
    ((DynArray.length rects.t_points) = 1) &&
    (point_is_top (DynArray.get rects.t_points 0))

  (** Whether every minimal point of a set satisfies a predicate. *)
  let for_all pred rects =
    DynArrayU.for_all pred rects.t_points

  (** Whether at least one minimal point of a set satisfies a predicate. *)
  let exists pred rects =
    DynArrayU.exists pred rects.t_points

  (** Applies [fn] to every minimal point of a set. *)
  let iter fn rects =
    DynArray.iter fn rects.t_points

  let fold_left fn init rects =
    DynArray.fold_left fn init rects.t_points

  (** Creates a set from a single point.
      
      [of_point pt] represents the upward closure of [pt].*)
  let of_point pt =
    let dim = point_dim pt in
    if point_is_bot pt then
      mk_empty dim
    else
      { t_dim = point_dim pt; t_points = DynArray.init 1 (fun _ -> pt) } 

  let of_n_bound rect_dim dim bound =
    of_point (point_of_n_bound rect_dim dim bound)
  
  (** Creates the greatest set of a given dimension.
  
      It is the upward closure of the greatest point (every [int] coordinate is
      [min_int], every [N.t] coordinate is [N.minus_inf]). *)
  let mk_top dim =
    let top_point = mk_top_point dim in
    { t_dim = dim; t_points = DynArray.init 1 (fun _ -> top_point) } 

  (** Inclusion order on sets.
      
      [leq rects1 rects2] iff [rects1] is non-strictly included in [rects2].
      That is, if for every minimal point in [rects1], there exists a
      non-strictly-greater point in [rects2]. *)
  let leq rects1 rects2 =
    for_all (fun pt1 -> exists (fun pt2 -> point_leq pt1 pt2) rects2) rects1

  (** Whether two sets are equal. *)
  let equal rects1 rects2 =
    (DynArray.length rects1.t_points = DynArray.length rects2.t_points) &&
    (for_all (fun pt1 -> exists (fun pt2 -> pt1 = pt2) rects2) rects1)

  let point_array_add points new_pt =
    let rec add_rec i =
      if i >= DynArray.length points then
        DynArray.add points new_pt
      else
        let cmp = point_compare (DynArray.get points i) new_pt in
        if cmp_is_geq cmp then
          ()
        else if cmp_is_leq cmp then begin
          DynArrayU.swap_remove points i;
          add_rec i
        end else (* if cmp_is_incomp cmp *)
          add_rec (i + 1)
    in
    if not (point_is_bot new_pt) then
      add_rec 0
    else
      ()

  (** Creates a set from an array of points.
   
      [of_point_array dim arr] is the union of upward closures of the points in
      [arr].
      Points do not have to be incomparable; non-minimal points will be filtered
      out.
      Every point has to be of dimension [dim]. *)
  let of_points dim arr =
    let points = DynArray.make (Array.length arr) in
    Array.iter
      (fun pt -> 
        check_arg (point_dim pt = dim) "All points should have the declared dimension.";
        point_array_add points pt)
      arr;
    { t_dim = dim; t_points = points }

  (** Formats a set. *)
  let format fmt rects =
    pp_open_box fmt 0;
    pp_print_string fmt "[";
    format_dynarray
      ~print_sep:(fun fmt -> pp_print_string fmt ";"; pp_print_space fmt ())
      format_point fmt rects.t_points;
    pp_print_string fmt "]";
    pp_close_box fmt ()

  (** Compares two sets and returns their union.
   
      If the result would be equal to one of the arguments, the function will
      return a reference to that argument and not a copy.
      That is, [union_cmp rects1 rects2] returns:
        - [(cmp_eq, rects1)] if [rects1] equals [rects2];
        - [(cmp_gt, rects1)] if [rects1] is stictly greater than [rects2].
          In this and the previous case, the implementation does not allocate extra
          memory;
        - [(cmp_lt, rects2)] if [rects1] is strictly less than [rects2].
          The implementation will allocate extra memory to store intermediate data;
        - otherwise, it returns [cmp_incomp] and a new set which is a union of
          [rects1] and [rects2]. *)
  let union_cmp rects1 rects2 =
    Counters.increment Counters.union_calls;
    if rects1 == rects2 then
      (cmp_eq, rects1)
    else if is_empty rects1 then begin
      if is_empty rects2 then
        (cmp_eq, rects1)
      else
        (cmp_lt, rects2)
    end else if is_empty rects2 then
      (cmp_gt, rects1)
    else begin
      let dim1 = dim rects1 in
      check_arg ((dim rects2) = dim1) "Sets should have same dimensions.";
      let points2 = rects2.t_points in
      let len2 = DynArray.length points2 in
      let rec union_loop points old_len new_len eq_n pos2 =
        if old_len = eq_n then
          (cmp_lt, rects2)
        else if pos2 >= len2 then
          (cmp_incomp, { t_dim = dim1; t_points = points })
        else
          let pt2 = DynArray.get points2 pos2 in
          union_loop_inner points old_len new_len eq_n 0 pos2 pt2
      and union_loop_inner points old_len new_len eq_n pos1 pos2 pt2 =
        if pos1 >= old_len then 
          union_append points old_len new_len eq_n pos2 pt2
        else
          let pt1 = DynArray.get points pos1 in
          let cmp = point_compare pt1 pt2 in
          if cmp_is_eq cmp then
            union_loop points old_len new_len (eq_n + 1) (pos2 + 1)
          else if cmp_is_gt cmp then
            union_loop points old_len new_len eq_n (pos2 + 1)
          else if cmp_is_lt cmp then
            union_replace points old_len new_len eq_n pos1 pos2 pt2
          else (* if cmp_is_incomp cmp *)
            union_loop_inner points old_len new_len eq_n (pos1 + 1) pos2 pt2
      and union_append points old_len new_len eq_n pos2 pt2  =
        if old_len = eq_n then
          (cmp_lt, rects2)
        else begin
          DynArray.add points pt2;
          union_loop points old_len (new_len + 1) eq_n (pos2 + 1)
        end
      and union_replace points old_len new_len eq_n pos1 pos2 pt2 =
        if old_len = eq_n + 1 then
          (cmp_lt, rects2)
        else begin
          (* NOTE: Not adding pt2 to points immediately. Will do this in
             union_loop_innerafter the whole old part is scanned. *)
          DynArrayU.swap points pos1 (old_len - 1);
          DynArrayU.swap_remove points (old_len - 1);
          union_loop_inner points (old_len - 1) new_len eq_n pos1 pos2 pt2
        end
      in
      let points1 = rects1.t_points in
      let len1 = DynArray.length points1 in
      let rec check_geq eq_n pos2 =
        if pos2 >= len2 then
          if eq_n = len2 && len2 = len1 then (cmp_eq, rects1) else (cmp_gt, rects1)
        else
          let pt2 = DynArray.get points2 pos2 in
          let rec check_geq_inner pos1 =
            if pos1 >= len1 then
              (* TODO: When copying, perhaps reserve space for len2 extra elements. *)
              let points1_copy = DynArray.copy points1 in
              union_append points1_copy len1 0 eq_n pos2 pt2
            else
              let pt1 = DynArray.get points1 pos1 in
              let cmp = point_compare pt1 pt2 in
              if cmp_is_eq cmp then
                check_geq (eq_n + 1) (pos2 + 1)
              else if cmp_is_gt cmp then
                check_geq eq_n (pos2 + 1)
              else if cmp_is_lt cmp then
                (* TODO: When copying, perhaps reserve space for len2 extra elements. *)
                let points1_copy = DynArray.copy points1 in
                union_replace points1_copy len1 0 eq_n pos1 pos2 pt2
              else (* if cmp_is_incomp cmp *)
                check_geq_inner (pos1 + 1)
          in
          check_geq_inner 0
      in
      check_geq 0 0
    end

  (** Union of two sets.
   
      Calls [union_cmp] and thus has same guarantees about the result. *)
  let union rects1 rects2 =
    let (_, union) = union_cmp rects1 rects2 in
    union

  (** Produces the intersection of the two sets.
     
      Does NOT provide the guarantees that [union] provides. *)
  let intersect rects1 rects2 =
    Counters.increment Counters.intersect_calls;
    let dim1 = dim rects1 in
    check_arg (dim1 = dim rects2) "Sets should have the same dimension.";
    let points = DynArray.make
      (max (DynArray.length rects1.t_points) (DynArray.length rects2.t_points)) in
    DynArrayU.iter_cartesian
      (fun pt1 pt2 ->
        point_array_add points (point_intersect pt1 pt2))
      rects1.t_points
      rects2.t_points;
    { t_dim = dim1; t_points = points }

  let point_negate_flip pt =
    let ints_len, ns_len = point_dim pt in
    let result = DynArray.make (ints_len + ns_len) in
    for i = 0 to ints_len - 1 do
      let bound = point_get_int pt i in
      if bound > min_int then
        DynArray.add result (point_of_int_bound (ints_len, ns_len) i (- bound))
      else ()
    done;
    for i = 0 to ns_len - 1 do
      Counters.increment Counters.intersect_calls;
      let bound = point_get_n pt i in
      if N.gt bound N.minus_inf then
        DynArray.add result (point_of_n_bound (ints_len, ns_len) i (N.neg bound))
      else ()
    done;
    { t_dim = (ints_len, ns_len); t_points = result }

  let negate_flip rects =
    let dim = dim rects in
    if is_empty rects then
      mk_top dim
    else begin
      let len = length rects in
      let rec negate_flip_rec result i =
        if i >= len then
          result
        else
          negate_flip_rec
            (intersect result (point_negate_flip (get rects i)))
            (i + 1)
      in
      negate_flip_rec (point_negate_flip (get rects 0)) 1
    end

  (** Formats a set to string. *)
  let to_string rects =
    asprintf "%a" format rects

end
