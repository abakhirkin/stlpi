open U
open Format
open FormatU

module Make(N: Numbers.T) = struct

  type var_t = {
    var_id: int
  } [@@unboxed]

  let var_id var =
    var.var_id

  let mk_var id =
    { var_id = id }

  type param_t = {
    param_id: int
  } [@@unboxed]

  let param_id param =
    param.param_id

  let mk_param id =
    { param_id = id }

  type space_const_t = 
    | SLit of N.t
    | SParam of param_t

  type time_const_t =
    | TLit of int
    | TParam of param_t

  type intvl_t = {
    intvl_lower: time_const_t;
    intvl_upper: time_const_t
  }

  let intvl_lower intvl = 
    intvl.intvl_lower

  let intvl_upper intvl =
    intvl.intvl_upper

  let mk_intvl lower upper =
    begin match lower with
    | TLit n ->
        check_arg (n >= 0) "Lower bound should be >= 0.";
        check_arg (n < max_int) "Lower bound cannot be infinity."
    | _ -> ()
    end;
    begin match upper with
    | TLit n -> check_arg (n >= 0) "Upper bound should be >= 0."
    | _ -> ()
    end;
    begin match (lower, upper) with
    | (TLit ln, TLit un) ->
        check_arg (ln <= un) "Lower bound should be <= than the upper bound."
    | _ -> ()
    end;
    { intvl_lower = lower; intvl_upper = upper }

  let intvl_add intvl1 intvl2 =
    match intvl1, intvl2 with
    | {intvl_lower = TLit lower1; intvl_upper = TLit upper1},
      {intvl_lower = TLit lower2; intvl_upper = TLit upper2} ->
        if upper1 = max_int || upper2 = max_int then
          mk_intvl (TLit (lower1 + lower2)) (TLit max_int)
        else
          mk_intvl (TLit (lower1 + lower2)) (TLit (upper1 + upper2))
    | _ -> invalid_arg "Cannot add intervals with parameter bounds."

  let intvl_has_parameters intvl =
    match intvl with
    | { intvl_lower = TLit _; intvl_upper = TLit _ } -> false
    | _ -> true

  let intvl_can_add intvl1 intvl2 =
    not (intvl_has_parameters intvl1) && not (intvl_has_parameters intvl2)

  (* TODO: Wrap this in a record and suggest that users use mk_* functions
     and don't invoke the constructors directly. *)
  type formula_t =
    | Leq of var_t * space_const_t
    | Geq of var_t * space_const_t    
    | F of intvl_t * formula_t
    | G of intvl_t * formula_t
    | U of intvl_t * formula_t * formula_t
    | R of intvl_t * formula_t * formula_t
    | True
    | False
    | And of formula_t array
    | Or of formula_t array
    | Not of formula_t

  let mk_and args =
    (* TODO: Flatten ands in arguments. *)
    match args with
    | [| |] -> True
    | [| arg |] -> arg
    | _ -> And args

  let mk_or args =
    (* TODO: Flatter ors in arguments. *)
    match args with
    | [| |] -> False
    | [| arg |] -> arg
    | _ -> Or args

  let mk_not ?(flatten=true) arg =
    match arg with
    | Not inner_arg ->
        if flatten then inner_arg else Not arg
    | _ -> Not arg

  let mk_f ?(flatten=true) intvl arg =
    match arg with
    | F (inner_intvl, inner_arg) ->
        if flatten && (intvl_can_add intvl inner_intvl) then
          F (intvl_add intvl inner_intvl, inner_arg)
        else
          F (intvl, arg)
    | _ -> F (intvl, arg)

  let mk_g ?(flatten=true) intvl arg =
    match arg with
    | G (inner_intvl, inner_arg) ->
        if flatten && (intvl_can_add intvl inner_intvl) then
          G (intvl_add intvl inner_intvl, inner_arg)
        else
          G (intvl, arg)
    | _ -> G (intvl, arg)

  let mk_until ?(flatten=true) intvl arg1 arg2 =
    match arg1 with
    | True ->
        if flatten then
          mk_f intvl arg2
        else
          U (intvl, arg1, arg2)
    | _ -> U (intvl, arg1, arg2)

  let mk_r ?(flatten=true) intvl arg1 arg2 =
    match arg1 with
    | False ->
        if flatten then
          mk_g intvl arg2
        else
          R(intvl, arg1, arg2)
    | _ -> R (intvl, arg1, arg2)

  let mk_nnf formula =
    let rec mk_nnf_rec negated formula =
      match formula with
      | Leq (var, const) -> if not negated then formula else Geq (var, const)
      | Geq (var, const) -> if not negated then formula else Leq (var, const)
      | True -> if not negated then formula else False
      | False -> if not negated then formula else True
      | F (intvl, arg_formula) ->
        if not negated then
          mk_f intvl (mk_nnf_rec negated arg_formula)
        else
          mk_g intvl (mk_nnf_rec negated arg_formula)
      | G (intvl, arg_formula) ->
        if not negated then
          mk_g intvl (mk_nnf_rec negated arg_formula)
        else
          mk_f intvl (mk_nnf_rec negated arg_formula)
      | U (intvl, arg_formula1, arg_formula2) ->
          if not negated then
            mk_until intvl
              (mk_nnf_rec negated arg_formula1)
              (mk_nnf_rec negated arg_formula2)
          else
            mk_r intvl
              (mk_nnf_rec negated arg_formula1)
              (mk_nnf_rec negated arg_formula2)
      | R (intvl, arg_formula1, arg_formula2) ->
          if not negated then
            mk_r intvl
              (mk_nnf_rec negated arg_formula1)
              (mk_nnf_rec negated arg_formula2)
          else
            mk_until intvl
              (mk_nnf_rec negated arg_formula1)
              (mk_nnf_rec negated arg_formula2)
      | And arg_formulas ->
        if not negated then
          mk_and (Array.map (mk_nnf_rec negated) arg_formulas)
        else
          mk_or (Array.map (mk_nnf_rec negated) arg_formulas)
      | Or arg_formulas ->
        if not negated then
          mk_or (Array.map (mk_nnf_rec negated) arg_formulas)
        else
          mk_and (Array.map (mk_nnf_rec negated) arg_formulas)
      | Not arg_formula -> mk_nnf_rec (not negated) arg_formula
    in
    mk_nnf_rec false formula

  let rec push_temporal formula =
    match formula with
    | F (intvl, arg_formula) ->
      let pushed_arg_formula = push_temporal arg_formula in
      begin match pushed_arg_formula with
      | Or inner_args -> mk_or (Array.map (mk_f intvl) inner_args)
      | _ -> mk_f intvl pushed_arg_formula
      end
    | G (intvl, arg_formula) ->
       let pushed_arg_formula = push_temporal arg_formula in
       begin match pushed_arg_formula with
        | And inner_args -> mk_and (Array.map (mk_g intvl) inner_args)
        | _ -> mk_g intvl pushed_arg_formula
       end
    | And args ->
        mk_and (Array.map push_temporal args)
    | Or args ->
        mk_or (Array.map push_temporal args)
    | _ ->
        formula

  let format_var fmt var =
    pp_open_box fmt 0;
    pp_print_string fmt "x";
    pp_print_int fmt (var_id var);
    pp_close_box fmt ()

  let format_space_const fmt const =
    pp_open_box fmt 0;
    begin match const with
    | SLit n -> N.format fmt n
    | SParam param ->
        pp_print_string fmt "p";
        pp_print_int fmt (param_id param)
    end;
    pp_close_box fmt ()

  let format_time_const fmt const =
    pp_open_box fmt 0;
    begin match const with
    | TLit n ->
        if n = max_int then
          pp_print_string fmt "inf"
        else
          pp_print_int fmt n
    | TParam param ->
        pp_print_string fmt "p";
        pp_print_int fmt (param_id param)
    end;
    pp_close_box fmt ()

  let format_interval_sexp fmt intvl =
    pp_open_box fmt 0;
    pp_print_string fmt "(";
    format_time_const fmt (intvl_lower intvl);
    pp_print_space fmt ();
    format_time_const fmt (intvl_upper intvl);
    pp_print_string fmt ")";
    pp_close_box fmt ()

  let rec format_formula_sexp fmt formula =
    pp_open_box fmt 0;
    begin match formula with
    | True -> pp_print_string fmt "true"
    | False -> pp_print_string fmt "false"
    | Leq (var, const) ->
        pp_print_string fmt "(";
        pp_print_string fmt "<=";
        pp_print_string fmt " ";
        format_var fmt var;
        pp_print_space fmt ();
        format_space_const fmt const;
        pp_print_string fmt ")"
    | Geq (var, const) ->
        pp_print_string fmt "(";
        pp_print_string fmt ">=";
        pp_print_string fmt " ";
        format_var fmt var;
        pp_print_space fmt ();
        format_space_const fmt const;
        pp_print_string fmt ")"
    | F (intvl, arg_formula) ->
        pp_print_string fmt "(";
        pp_print_string fmt "F";
        pp_print_space fmt ();
        format_interval_sexp fmt intvl;
        pp_print_space fmt ();
        format_formula_sexp fmt arg_formula;
        pp_print_string fmt ")"
    | G (intvl, arg_formula) ->
        pp_print_string fmt "(";
        pp_print_string fmt "G";
        pp_print_space fmt ();
        format_interval_sexp fmt intvl;
        pp_print_space fmt ();
        format_formula_sexp fmt arg_formula;
        pp_print_string fmt ")"
    | U (intvl, arg_formula1, arg_formula2) ->
        pp_open_box fmt 3;
        pp_print_string fmt "(";
        pp_print_string fmt "U";
        pp_print_space fmt ();
        format_interval_sexp fmt intvl;
        pp_print_space fmt ();
        format_formula_sexp fmt arg_formula1;
        pp_print_space fmt ();
        format_formula_sexp fmt arg_formula2;
        pp_print_string fmt ")";
        pp_close_box fmt ()
    | R (intvl, arg_formula1, arg_formula2) ->
        pp_open_box fmt 3;
        pp_print_string fmt "(";
        pp_print_string fmt "R";
        pp_print_space fmt ();
        format_interval_sexp fmt intvl;
        pp_print_space fmt ();
        format_formula_sexp fmt arg_formula1;
        pp_print_space fmt ();
        format_formula_sexp fmt arg_formula2;
        pp_print_string fmt ")";
        pp_close_box fmt ()
    | And arg_formulas ->
        pp_open_box fmt 5;
        pp_print_string fmt "(";
        pp_print_string fmt "and";
        pp_print_string fmt " ";
        format_array format_formula_sexp fmt arg_formulas;
        pp_print_string fmt ")";
        pp_close_box fmt ()
    | Or arg_formulas ->
        pp_open_box fmt 4;
        pp_print_string fmt "(";
        pp_print_string fmt "or";
        pp_print_string fmt " ";
        format_array format_formula_sexp fmt arg_formulas;
        pp_print_string fmt ")";
        pp_close_box fmt ()
    | Not arg_formula ->
        pp_print_string fmt "(";
        pp_print_string fmt "not";
        pp_print_space fmt ();
        format_formula_sexp fmt arg_formula;
        pp_print_string fmt ")"
    end;
    pp_close_box fmt ()

  let format_formula = format_formula_sexp
  
  let formula_to_string formula =
    asprintf "%a" format_formula formula

end

module Fpstl = Make(Numbers.Float)
