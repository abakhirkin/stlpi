open Format
open FormatU

type counter_t = {
  counter_id: int;
  counter_name: string;
  counter_unit_name: string
} 

let counter_id counter =
  counter.counter_id

let counter_name counter = 
  counter.counter_name

let counter_unit_name counter =
  counter.counter_unit_name

let counters = DynArray.create ()
let values = DynArray.create ()

let mk_counter ?(unit_name="") name =
  let counter =
    { counter_id = DynArray.length counters;
      counter_name = name;
      counter_unit_name = unit_name} in
  DynArray.add counters counter;
  DynArray.add values 0;
  counter

let fcounters = DynArray.create ()
let fvalues = DynArray.create ()

let mk_fcounter ?(unit_name="") name =
  let fcounter =
    { counter_id = DynArray.length fcounters;
      counter_name = name;
      counter_unit_name = unit_name } in
  DynArray.add fcounters fcounter;
  DynArray.add fvalues 0.;
  fcounter

let union_calls = mk_counter "'Union' calls"
let intersect_calls = mk_counter "'Intersect' calls"

let identify_time = mk_fcounter ~unit_name:"s" "Runtime of 'identify'"

let increment ?(step=1) counter =
  let id = counter_id counter in
  DynArray.set values id ((DynArray.get values id) + step)

let fset fcounter v =
  let id = counter_id fcounter in
  DynArray.set fvalues id v

let format_counter fmt counter =
  pp_open_box fmt 0;
  pp_print_string fmt (counter_name counter);
  pp_print_string fmt ":";
  pp_print_space fmt ();
  pp_print_int fmt (DynArray.get values (counter_id counter));
  pp_print_string fmt (counter_unit_name counter);
  pp_close_box fmt ()

let format_fcounter fmt fcounter =
  pp_open_box fmt 0;
  pp_print_string fmt (counter_name fcounter);
  pp_print_string fmt ":";
  pp_print_space fmt ();
  pp_print_string fmt (sprintf "%g" (DynArray.get fvalues (counter_id fcounter)));
  pp_print_string fmt (counter_unit_name fcounter);
  pp_close_box fmt ()

let format_counters fmt () =
  pp_open_box fmt 0;
  format_dynarray_nl format_counter fmt counters;
  pp_print_newline fmt ();
  format_dynarray_nl format_fcounter fmt fcounters;
  pp_close_box fmt ()

