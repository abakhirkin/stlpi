open U
open OUnit2
open Format
open FormatU
module DynArray = Batteries.DynArray

module Pstl = Pstl.Make(Numbers.Float)
module Rects = URects.Make(Numbers.Float)
module PI = ParamIdent.Make(Numbers.Float)

open Pstl

(* TODO: Tests for negate_flip. *)
(* TODO: Tests for correct handling of empty signals. *)

let test_parse_formula _ =
  let f1 = PstlU.parse_string
    "(or (<= x0 0) (and (>= x1 p1) (F (0 3) (and (>= x0 p0) (>= x1 1.5)))))"
  in
  let x0 = mk_var 0 in
  let x1 = mk_var 1 in
  let p0 = mk_param 0 in
  let p1 = mk_param 1 in
  let expected1 =
    Or [|
      Leq(x0, SLit 0.);
      And [|
        Geq(x1, SParam p1);
        F (mk_intvl (TLit 0) (TLit 3),
          And [|
            Geq(x0, SParam p0);
            Geq(x1, SLit 1.5) |] ) |] |]
  in
  assert_equal ~printer:formula_to_string expected1 f1

let test_format_formula _ =
  let x0 = mk_var 0 in
  let x1 = mk_var 1 in
  let p0 = mk_param 0 in
  let p1 = mk_param 1 in
  let f1 = Leq (x0, SParam p0) in
  let f2 = Not f1 in
  let f3 = F (mk_intvl (TLit 0) (TParam p1), f2) in
  let f4 = And [|f3; Geq (x1, SLit 0.0)|] in
  let f5 = F (mk_intvl (TLit 0) (TLit max_int), f2) in
  let s1 = asprintf "%a" format_formula f1 in
  let s2 = asprintf "%a" format_formula f2 in
  let s3 = asprintf "%a" format_formula f3 in
  let s4 = asprintf "%a" format_formula f4 in
  assert_equal ~printer:(fun s->s) "(<= x0 p0)" s1;
  assert_equal ~printer:(fun s->s) "(not (<= x0 p0))" s2;
  assert_equal ~printer:(fun s->s) "(F (0 p1) (not (<= x0 p0)))" s3;
  assert_equal ~printer:(fun s->s) "(and (F (0 p1) (not (<= x0 p0))) (>= x1 0))" s4;
  assert_equal ~printer:(fun s->s)
    "(F (0 inf) (not (<= x0 p0)))"
    (formula_to_string f5)

let test_mk_nnf _ =
  let x0 = mk_var 0 in
  let x1 = mk_var 1 in
  let p0 = mk_param 0 in
  let p1 = mk_param 1 in
  let intvl1 = mk_intvl (TLit 0) (TParam p1) in
  let f1 = True in
  let nnf1 = True in
  let f2 = Not True in
  let nnf2 = False in
  let f3 = Leq (x0, SParam p0) in
  let f4 = Geq (x1, SLit 0.0) in
  let f5 = Not (Or [|Not f3; f4|]) in
  let nnf5 = And [|f3; Leq (x1, SLit 0.0)|] in
  let f6 = Not (F (intvl1, G (intvl1, f3))) in
  let nnf6 = G (intvl1, F (intvl1, Geq (x0, SParam p0))) in
  assert_equal ~printer:formula_to_string nnf1 (mk_nnf f1);
  assert_equal ~printer:formula_to_string nnf2 (mk_nnf f2);
  assert_equal ~printer:formula_to_string nnf5 (mk_nnf f5);
  assert_equal ~printer:formula_to_string nnf6 (mk_nnf f6)

let test_push_temporal _ =
  let f1 = PstlU.parse_string "(G (0 inf) (and (<= x0 p0) (<= x1 0)))" in
  let expected1 = PstlU.parse_string
    "(and (G (0 inf) (<= x0 p0)) (G (0 inf) (<= x1 0)))"
  in
  let f2 = PstlU.parse_string "(F (0 p0) (F (1 3) (or (<= x0 0) (<= x1 p1))))" in
  let expected2 = PstlU.parse_string
    "(or (F (0 p0) (F (1 3) (<= x0 0))) (F (0 p0) (F (1 3) (<= x1 p1))))" in
  let f3 = PstlU.parse_string "(F (2 4) (or (F (1 3) (<= x0 0)) (<= x1 p1)))" in
  let expected3 = PstlU.parse_string "(or (F (3 7) (<= x0 0)) (F (2 4) (<= x1 p1)))" in
  assert_equal ~printer:formula_to_string expected1 (push_temporal f1);
  assert_equal ~printer:formula_to_string expected2 (push_temporal f2);
  assert_equal ~printer:formula_to_string expected3 (push_temporal f3)

let test_series_array _ =
  let samples1 = [| 0.; 1.; 0.; 1.; 0.; 1.; 0.; 1.|] in
  let series1 = Series.of_sample_array (=) samples1 in
  let new_samples1 = Series.to_sample_array series1 in
  let samples2 = [| 0.; 0.; 1.; 1.; 2.; 2.; 2.; 1.; 0. |] in
  let series2 = Series.of_sample_array (=) samples2 in
  let new_samples2 = Series.to_sample_array series2 in
  assert_equal samples1 new_samples1;
  assert_equal samples2 new_samples2

let test_point_compare _ =
  let pt1 = Rects.mk_point [|0; 0; 0|] [|0.; 0.; 0.|] in
  let pt2 = Rects.mk_point [|1; 0; 0|] [|0.; 0.; 1.|] in
  let pt3 = Rects.mk_point [|1; 0; 1|] [|1.; 0.; 1.|] in
  let pt4 = Rects.mk_point [|0; 0; 0|] [|0.; 0.; 1.|] in
  let pt5 = Rects.mk_point [|0; 0; 0|] [||] in
  let pt6 = Rects.mk_point [|0; 1; 0|] [||] in
  let pt7 = Rects.mk_point [||] [|1.; 0.; 0.|] in
  let pt8 = Rects.mk_point [||] [|2.; 0.; 1.|] in
  let pt9 = Rects.mk_point [|1; 0; 0|] [|1.; 1.; 1.|] in
  assert_bool "(0 0 0 0 0 0) <= (0 0 0; 0 0 0)" (Rects.point_leq pt1 pt1);
  assert_equal ~printer:string_of_cmp cmp_eq (Rects.point_compare pt1 pt1);
  assert_bool "(1 0 0; 0 0 1) <= (0 0 0; 0 0 0)" (Rects.point_leq pt2 pt1);
  assert_bool "not (1 0 0; 0 0 1) >= (0 0 0; 0 0 0)"
    (not (Rects.point_leq pt1 pt2));
  assert_equal ~printer:string_of_cmp cmp_lt (Rects.point_compare pt2 pt1);
  assert_equal ~printer:string_of_cmp cmp_gt (Rects.point_compare pt1 pt2);
  assert_bool "(1 0 1; 1 0 1) <= (1 0 0; 0 0 1)" (Rects.point_leq pt3 pt2);
  assert_bool "not (1 0 1; 1 0 1) >= (1 0 0; 0 0 1)"
    (not (Rects.point_leq pt2 pt3));
  assert_equal ~printer:string_of_cmp cmp_lt (Rects.point_compare pt3 pt2);
  assert_equal ~printer:string_of_cmp cmp_gt (Rects.point_compare pt2 pt3);
  assert_bool "(0 0 0; 0 0 1) <= (0 0 0; 0 0 0)" (Rects.point_leq pt4 pt1);
  assert_bool "not (0 0 0; 0 0 1) >= (0 0 0; 0 0 0)"
    (not (Rects.point_leq pt1 pt4));
  assert_equal ~printer:string_of_cmp cmp_lt (Rects.point_compare pt4 pt1);
  assert_equal ~printer:string_of_cmp cmp_gt (Rects.point_compare pt1 pt4);
  assert_bool "(0 1 0; ) <= (0 0 0; )" (Rects.point_leq pt6 pt5);
  assert_equal ~printer:string_of_cmp cmp_lt (Rects.point_compare pt6 pt5);
  assert_equal ~printer:string_of_cmp cmp_gt (Rects.point_compare pt5 pt6);
  assert_bool "(; 2 0 1) <= (; 1 0 0)" (Rects.point_leq pt8 pt7);
  assert_equal ~printer:string_of_cmp cmp_lt (Rects.point_compare pt8 pt7);
  assert_equal ~printer:string_of_cmp cmp_gt (Rects.point_compare pt7 pt8);
  assert_bool "not (1 0 1; 1 0 1) >= (1 0 0; 1 1 1)"
    (not (Rects.point_leq pt9 pt3));
  assert_bool "not (1 0 1; 1 0 1) <= (1 0 0; 1 1 1)"
    (not (Rects.point_leq pt3 pt9));
  assert_equal ~printer:string_of_cmp cmp_incomp (Rects.point_compare pt9 pt3);
  assert_equal ~printer:string_of_cmp cmp_incomp (Rects.point_compare pt3 pt9)

let test_of_points _ =
  let pt1 = Rects.mk_point [|2|] [|3.|] in
  let pt2 = Rects.mk_point [|4|] [|0.|] in
  let pt3 = Rects.mk_point [|3|] [|2.|] in
  let pt4 = Rects.mk_point [|1|] [|1.|] in
  let rects1 = Rects.of_points (1,1) [| pt1; pt2; pt3; pt4 |] in
  let expected1 = Rects.of_points (1,1) [| pt2; pt4 |] in
  assert_equal ~cmp:Rects.equal expected1 rects1

let test_leq _ =
  let empty = Rects.mk_empty (1,1) in
  let top = Rects.mk_top (1,1) in
  let pt1 = Rects.mk_point [|2|] [|3.|] in
  let pt2 = Rects.mk_point [|4|] [|0.|] in
  let pt3 = Rects.mk_point [|3|] [|2.|] in
  let pt4 = Rects.mk_point [|1|] [|1.|] in
  let rects1 = Rects.of_points (1,1) [| pt1; pt2; pt3 |] in
  let rects2 = Rects.of_points (1,1) [| pt2; pt4 |] in
  assert_bool "empty <= top" (Rects.leq empty top);
  assert_bool "not empty >= top" (not (Rects.leq top empty));
  assert_bool "[(4 0); (1 1)] <= top" (Rects.leq rects2 top);
  assert_bool "not [(4 0); (1 1)] >= top" (not (Rects.leq top rects2));
  assert_bool "empty <= [(4 0); (1 1)]" (Rects.leq empty rects2);
  assert_bool "not empty >= [(4 0); (1 1)]" (not (Rects.leq rects2 empty));
  assert_bool "[(2 3); (4 0); (3 2)] <= [(4 0); (1 1)]"
    (Rects.leq rects1 rects2);
  assert_bool "not [(2 3); (4 0); (3 2)] >= [(4 0); (1 1)]"
    (not (Rects.leq rects2 rects1))

let test_intersect _ =
  let pt1 = Rects.mk_point [|1|] [|0.|] in
  let pt2 = Rects.mk_point [|0|] [|1.|] in
  let pt3 = Rects.mk_point [|1|] [|1.|] in
  let pt4 = Rects.mk_point [|2|] [|0.|] in
  let pt5 = Rects.mk_point [|0|] [|2.|] in
  let pt6 = Rects.mk_point [|0|] [|3.|] in
  let pt7 = Rects.mk_point [|1|] [|2.|] in
  let pt8 = Rects.mk_point [|2|] [|1.|] in
  let pt9 = Rects.mk_point [|3|] [|0.|] in
  let pt10 = Rects.mk_point [|2|] [|2.|] in
  let empty = Rects.mk_empty (1,1) in
  let top = Rects.mk_top (1,1) in
  let rects1 = Rects.of_points (1,1) [| pt1 |] in
  let rects2 = Rects.of_points (1,1) [| pt2 |] in
  let rects3 = Rects.of_points (1,1) [| pt3; pt4; pt5 |] in
  let rects4 = Rects.of_points (1,1) [| pt6; pt7 |] in
  let rects5 = Rects.of_points (1,1) [| pt8; pt9; |] in
  let rects6 = Rects.of_points (1,1) [| pt3 |] in
  let expected12 = Rects.of_points (1,1) [| pt3 |] in
  let expected45 = Rects.of_points (1,1) [| pt10 |] in
  (* NOTE: 'Intersect' does not provide the same guarantees as 'union'. *)
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    empty
    (Rects.intersect empty rects3);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    empty
    (Rects.intersect rects3 empty);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    rects3
    (Rects.intersect top rects3);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    rects3
    (Rects.intersect rects3 top);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    expected12
    (Rects.intersect rects1 rects2);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    expected12
    (Rects.intersect rects2 rects1);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    expected45
    (Rects.intersect rects4 rects5);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    expected45
    (Rects.intersect rects5 rects4);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    rects6
    (Rects.intersect rects3 rects6);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    rects6
    (Rects.intersect rects6 rects3)

let test_union _ =
  let pt1 = Rects.mk_point [|2|] [|3.|] in
  let pt2 = Rects.mk_point [|4|] [|0.|] in
  let pt3 = Rects.mk_point [|3|] [|2.|] in
  let pt4 = Rects.mk_point [|1|] [|1.|] in
  let pt5 = Rects.mk_point [|0|] [|1.|] in
  let pt6 = Rects.mk_point [|1|] [|0.|] in
  let pt7 = Rects.mk_point [|0|] [|4.|] in
  let empty = Rects.mk_empty (1,1) in
  let top = Rects.mk_top (1,1) in
  let rects1 = Rects.of_points (1,1) [| pt1; pt3 |] in
  let rects1copy = Rects.of_points (1,1) [| pt1; pt3 |] in
  let rects2 = Rects.of_points (1,1) [| pt2; pt4 |] in
  let expected12 = Rects.of_points (1,1) [| pt2; pt4 |] in
  let rects3 = Rects.of_points (1,1) [| pt1; pt2; pt3 |] in
  let rects4 = Rects.of_points (1,1) [| pt2; pt4 |] in
  let rects5 = Rects.of_points (1,1) [| pt5; pt6 |] in
  let rects5_reorder = Rects.of_points (1,1) [| pt6; pt5 |] in
  let rects6 = Rects.of_points (1,1) [| pt2; pt5 |] in
  let rects7 = Rects.of_points (1,1) [| pt6; pt7 |] in
  let expected67 = Rects.of_points (1,1) [| pt5; pt6 |] in
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    rects1 (Rects.union rects1 empty);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    rects1 (Rects.union empty rects1);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    top (Rects.union rects1 top);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    top (Rects.union top rects1);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    expected12 (Rects.union rects1 rects2);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    rects2 (Rects.union rects2 rects3);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    rects4 (Rects.union rects4 rects3);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    rects5 (Rects.union rects4 rects5);
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string
    expected67 (Rects.union rects6 rects7);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    rects1 (Rects.union rects1 rects1copy);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    empty (Rects.union empty empty);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    top (Rects.union empty top);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    top (Rects.union top empty);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    top (Rects.union top top);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    rects1copy (Rects.union rects1copy rects1);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    rects5 (Rects.union rects7 rects5);
  assert_equal ~cmp:(==) ~printer:Rects.to_string
    rects5_reorder (Rects.union rects7 rects5_reorder)

let test_union_cmp _ =
  let empty = Rects.mk_empty (1,1) in
  let top = Rects.mk_top (1,1) in
  let (cmp_et, union_et) = Rects.union_cmp empty top in
  let (cmp_te, union_te) = Rects.union_cmp top empty in
  let (cmp_ee, union_ee) = Rects.union_cmp empty empty in
  let (cmp_tt, union_tt) = Rects.union_cmp top top in
  assert_equal ~printer:string_of_cmp cmp_lt cmp_et;
  assert_equal ~cmp:(==) ~printer:Rects.to_string top union_et;
  assert_equal ~printer:string_of_cmp cmp_gt cmp_te;
  assert_equal ~cmp:(==) ~printer:Rects.to_string top union_te;
  assert_equal ~printer:string_of_cmp cmp_eq cmp_ee;
  assert_equal ~cmp:(==) ~printer:Rects.to_string empty union_ee;
  assert_equal ~printer:string_of_cmp cmp_eq cmp_tt;
  assert_equal ~cmp:(==) ~printer:Rects.to_string top union_tt;
  let pt1 = Rects.mk_point [|3|] [|0.|] in
  let pt2 = Rects.mk_point [|0|] [|3.|] in
  let rects1 = Rects.of_points (1,1) [| pt1; pt2 |] in
  let rects1copy = Rects.of_points (1,1) [| pt1; pt2 |] in
  let (cmp_e1, union_e1) = Rects.union_cmp empty rects1 in
  let (cmp_1e, union_1e) = Rects.union_cmp rects1 empty in
  let (cmp_t1, union_t1) = Rects.union_cmp top rects1 in
  let (cmp_1t, union_1t) = Rects.union_cmp rects1 top in
  let (cmp_11c, union_11c) = Rects.union_cmp rects1 rects1copy in
  assert_equal ~printer:string_of_cmp cmp_lt cmp_e1;
  assert_equal ~cmp:(==) ~printer:Rects.to_string rects1 union_e1;
  assert_equal ~printer:string_of_cmp cmp_gt cmp_1e;
  assert_equal ~cmp:(==) ~printer:Rects.to_string rects1 union_1e;
  assert_equal ~printer:string_of_cmp cmp_gt cmp_t1;
  assert_equal ~cmp:(==) ~printer:Rects.to_string top union_t1;
  assert_equal ~printer:string_of_cmp cmp_lt cmp_1t;
  assert_equal ~cmp:(==) ~printer:Rects.to_string top union_1t;
  assert_equal ~printer:string_of_cmp cmp_eq cmp_11c;
  assert_equal ~cmp:(==) ~printer:Rects.to_string rects1 union_11c;
  let pt3 = Rects.mk_point [|2|] [|1.|] in
  let pt4 = Rects.mk_point [|1|] [|2.|] in
  let rects2 = Rects.of_points (1,1) [| pt3; pt4; pt1 |] in
  let expected12 = Rects.of_points (1,1) [| pt1; pt2; pt3; pt4 |] in
  let (cmp_12, union_12) = Rects.union_cmp rects1 rects2 in
  let (cmp_21, union_21) = Rects.union_cmp rects2 rects1 in
  assert_equal ~printer:string_of_cmp cmp_incomp cmp_12;
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string expected12 union_12;
  assert_equal ~printer:string_of_cmp cmp_incomp cmp_21;
  assert_equal ~cmp:Rects.equal ~printer:Rects.to_string expected12 union_21


let max_cmp v1 v2 =
  if v1 = v2 then
    (cmp_eq, v1)
  else if v1 > v2 then
    (cmp_gt, v1)
  else (* if v1 < v2 *)
    (cmp_lt, v2)

let test_backshift_union _ =
  let dim1 = (1,1) in
  (* NOTE: We want to make sure that samples that are equal via Rects.equal are not
     equal via (==). *)
  let series1 = Series.of_sample_array Rects.equal
    [| Rects.of_n_bound dim1 0 1.;
       Rects.of_n_bound dim1 0 0.;
       Rects.of_n_bound dim1 0 1.;
       Rects.of_n_bound dim1 0 0.;
       Rects.of_n_bound dim1 0 1.;
       Rects.of_n_bound dim1 0 0.;
       Rects.of_n_bound dim1 0 1.;
       Rects.of_n_bound dim1 0 0. |]
  in
  let dim2 = (0,2) in
  let pt1_2 = Rects.mk_point [| |] [| 1.; -1. |] in
  let pt2_2 = Rects.mk_point [| |] [| 2.; -2. |] in
  let pt3_2 = Rects.mk_point [| |] [| 3.; -3. |] in
  let series2 = Series.of_sample_array Rects.equal
    [| Rects.of_point pt1_2; Rects.of_point pt2_2; Rects.of_point pt3_2 |]
  in
  let backshift1 =
    Series.backshift Rects.union_cmp Rects.equal (Rects.mk_empty dim1) 0 3 series1
  in
  let expected1 =
    Series.of_segment (Series.mk_segment 7 (Rects.of_n_bound dim1 0 0.))
  in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(Series.to_string Rects.format)
    expected1 backshift1;
  let backshift2 =
    Series.backshift Rects.union_cmp Rects.equal (Rects.mk_empty dim2) 0 max_int series2
  in
  let expected2 =
    Series.of_segment_array Rects.equal
      [| Series.mk_segment 0 (Rects.of_points dim2 [| pt1_2; pt2_2; pt3_2 |]);
         Series.mk_segment 1 (Rects.of_points dim2 [| pt2_2; pt3_2 |]);
         Series.mk_segment 2 (Rects.of_points dim2 [| pt3_2 |]) |]
  in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(Series.to_string Rects.format)
    expected2 backshift2

let test_backshift_int_max _ =
  let samples1 = [| 0; 1; 2; 3; 0; 1; 2; 3; 0; 1; 2; 3 |] in
  let samples2 = [| 0; 1; 2; 3; 2; 1; 0; 1; 2; 3; 2; 1; 0 |] in
  let samples3 = [| 0; 1; 2; 3; 4; 3; 2; 1; 0; 1; 2; 3; 4 |] in
  let samples4 = [| 0; 1; 4; 0; 1; 4; 0; 1; 4 |] in
  let samples5 = [| 1; 0; 1; 0; 1; 0; 1; 0; 1 |] in
  let samples6 = [| 0; 0; 1; 1; 2; 2; 1; 1; 0; 0; |] in
  let samples7 = [| 0; 0; 0; 1; 0; 1; |] in
  let series1 = Series.of_sample_array (=) samples1 in
  let series2 = Series.of_sample_array (=) samples2 in
  let series3 = Series.of_sample_array (=) samples3 in
  let series4 = Series.of_sample_array (=) samples4 in
  let series5 = Series.of_sample_array (=) samples5 in
  let series6 = Series.of_sample_array (=) samples6 in
  let series7 = Series.of_sample_array (=) samples7 in
  let backshift1 = Series.backshift max_cmp (=) min_int 1 3 series1 in
  let expected1 = Series.of_segment_array (=)
    [| Series.mk_segment 2 3;
       Series.mk_segment 3 2;
       Series.mk_segment 6 3;
       Series.mk_segment 7 2;
       Series.mk_segment 10 3;
       Series.mk_segment 11 min_int |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected1 backshift1;
  let backshift2 = Series.backshift max_cmp (=) min_int 0 max_int series2 in
  let expected2 = Series.of_segment_array (=)
    [| Series.mk_segment 9 3;
       Series.mk_segment 10 2;
       Series.mk_segment 11 1;
       Series.mk_segment 12 0 |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected2 backshift2;
  let backshift3 = Series.backshift max_cmp (=) min_int 2 4 series3 in
  let expected3 = Series.of_segment_array (=)
    [| Series.mk_segment 2 4;
       Series.mk_segment 3 3;
       Series.mk_segment 4 2;
       Series.mk_segment 5 1;
       Series.mk_segment 6 2;
       Series.mk_segment 7 3;
       Series.mk_segment 10 4;
       Series.mk_segment 12 min_int |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected3 backshift3;
  let backshift4 = Series.backshift max_cmp (=) min_int 5 8 series4 in
  let expected4 = Series.of_segment_array (=)
    [| Series.mk_segment 3 4;
       Series.mk_segment 8 min_int |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected4 backshift4;
  let backshift5 = Series.backshift max_cmp (=) min_int 0 1 series5 in
  let expected5 = Series.of_segment_array (=)
    [| Series.mk_segment 8 1 |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected5 backshift5;
  let backshift5_1 = Series.backshift max_cmp (=) min_int 0 2 series5 in
  let expected5_1 = Series.of_segment_array (=)
    [| Series.mk_segment 8 1 |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected5_1 backshift5_1;
  let backshift6 = Series.backshift max_cmp (=) min_int 1 3 series6 in
  let expected6 = Series.of_segment_array (=)
    [| Series.mk_segment 0 1;
       Series.mk_segment 4 2;
       Series.mk_segment 6 1;
       Series.mk_segment 8 0;
       Series.mk_segment 9 min_int |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected6 backshift6;
  let backshift7 = Series.backshift max_cmp (=) min_int 0 2 series7 in
  let expected7 = Series.of_segment_array (=)
    [| Series.mk_segment 0 0;
       Series.mk_segment 5 1 |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected7 backshift7

let test_until_int_max _ =
  let series_max13 = Series.of_sample_array (=) (Array.make 13 max_int) in
  let samples2 = [| 0; 1; 2; 3; 2; 1; 0; 1; 2; 3; 2; 1; 0 |] in
  let series1_1 = Series.of_sample_array (=) [| 0; 0; 1; 1; 2; 2; 1; 1 |] in
  let series1_2 = Series.of_sample_array (=) [| 1; 1; 0; 0; 2; 2; 2; 3 |] in
  let series2 = Series.of_sample_array (=) samples2 in
  let series3_1 = Series.of_sample_array (=) [| 1; 1; 1; 1; 1; 1; 0; 1; 1; 1; 1 |] in
  let series3_2 = Series.of_sample_array (=) [| 0; 0; 0; 1; 0; 0; 0; 0; 0; 1; 0 |] in
  let until1 = Series.until max min min_int (=) series1_1 series1_2 in
  let expected1 = Series.of_segment_array (=)
    [| Series.mk_segment 3 1;
       Series.mk_segment 6 2;
       Series.mk_segment 7 3 |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected1 until1;
  let until2 = Series.until max min min_int (=) series_max13 series2 in
  let expected2 = Series.backshift max_cmp (=) min_int 0 max_int series2 in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected2 until2;
  let until3 = Series.until max min min_int (=) series3_1 series3_2 in
  let expected3 = Series.of_segment_array (=)
    [| Series.mk_segment 3 1;
       Series.mk_segment 6 0;
       Series.mk_segment 9 1;
       Series.mk_segment 10 0 |]
  in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_int)
    expected3 until3

let test_format_param_bounds _ =
  let x0 = mk_var 0 in
  let x1 = mk_var 1 in
  let p0 = mk_param 0 in
  let p1 = mk_param 1 in
  let f1 = And [| Leq (x0, SParam p0); Geq (x1, SParam p1) |] in
  let empty = Rects.mk_empty (0,2) in
  let top = Rects.mk_top (0,2) in
  let ps = PI.mk_pspace f1 in
  (* NOTE: We actually don't know which dimension will be assigned to which parameter,
     but we guess that p0 will get dimension 0, since x0 <= p0 comes first in the
     array of arguments of 'and'. *)
  let pt1 = Rects.mk_point [||] [|0.; 0.|] in
  let pt2 = Rects.mk_point [||] [|1.; neg_infinity|] in
  let pt3 = Rects.mk_point [||] [|neg_infinity; 1.|] in
  let rects1 = Rects.of_points (0,2) [|pt1; pt2; pt3 |] in
  assert_equal ~printer:(fun s->s) "[false]" (PI.rects_to_string ps empty);
  assert_equal ~printer:(fun s->s) "[true]" (PI.rects_to_string ps top);
  assert_equal ~printer:(fun s->s)
    "[(p0 >= 0, -p1 >= 0); (p0 >= 1); (-p1 >= 1)]"
    (PI.rects_to_string ps rects1)

let test_identify_trivial _ =
  let samples1 = [| 0.; 1.; 0.; 1.; 0.; 1.; 0.; 1.|] in
  let samples2 = [| 0.; 1.; 2.; 1.; 0.; 1.; 2.|] in
  let series1 = Series.of_sample_array (=) samples1 in
  let series2 = Series.of_sample_array (=) samples2 in
  let signal1 = Signal.mk [| series1 |] in
  let signal2 = Signal.mk [| series2 |] in
  let x0 = mk_var 0 in
  let f2 = Leq (x0, SLit 0.) in
  let f3 = Geq (x0, SLit 2.) in
  (* Formulas True, False, f2, and f3 will have the same parameter space,
     with no parameters. *)
  let ps = PI.mk_pspace True in
  let top = Rects.mk_top (PI.pspace_rect_dim ps) in
  let empty = Rects.mk_empty (PI.pspace_rect_dim ps) in
  let _, v1_true = PI.identify True signal1 in
  let _, v1_false = PI.identify False signal1 in
  let _, v1_2 = PI.identify f2 signal1 in
  let _, v2_3 = PI.identify f3 signal2 in
  let expected1_2 = Series.of_sample_array Rects.equal
    [| top; empty; top; empty; top; empty; top; empty |]
  in
  let expected2_3 = Series.of_segment_array Rects.equal
    [| Series.mk_segment 1 empty;
       Series.mk_segment 2 top;
       Series.mk_segment 5 empty;
       Series.mk_segment 6 top |]
  in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    (Series.mk_const 7 top)
    v1_true;
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    (Series.mk_const 7 empty)
    v1_false;
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    expected1_2
    v1_2;
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    expected2_3
    v2_3

let test_identify_param_ineq _ =
  let samples1 = [| 0.; 1.; 0.; 1.; 0.; 1.; 0.|] in
  let samples2 = [| 0.; 1.; 2.; 2.; 1.; 0.; 0.|] in
  let series1 = Series.of_sample_array (=) samples1 in
  let series2 = Series.of_sample_array (=) samples2 in
  let signal1 = Signal.mk [| series1; series2 |] in
  let x1 = mk_var 0 in
  let x2 = mk_var 1 in
  let p1 = mk_param 1 in
  let p2 = mk_param 2 in
  let f1 = Leq (x1, SParam p1) in
  let f2 = Geq (x2, SParam p2) in
  let ps1, v1 = PI.identify f1 signal1 in
  let rect_dim1 = PI.pspace_rect_dim ps1 in
  let p1_dim = PI.pdata_dim (PI.pspace_data ps1 p1) in
  let p1_0 = Rects.of_n_bound rect_dim1 p1_dim 0. in
  let p1_1 = Rects.of_n_bound rect_dim1 p1_dim 1. in
  let expected1 = Series.of_segment_array Rects.equal
    [| Series.mk_segment 0 p1_0;
       Series.mk_segment 1 p1_1;
       Series.mk_segment 2 p1_0;
       Series.mk_segment 3 p1_1;
       Series.mk_segment 4 p1_0;
       Series.mk_segment 5 p1_1;
       Series.mk_segment 6 p1_0 |] in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps1)
    expected1 v1;
  let ps2, v2 = PI.identify f2 signal1 in
  let rect_dim2 = PI.pspace_rect_dim ps2 in
  let p2_dim = PI.pdata_dim (PI.pspace_data ps2 p2) in
  let p2_0 = Rects.of_n_bound rect_dim2 p2_dim 0. in
  let p2_1 = Rects.of_n_bound rect_dim2 p2_dim (- 1.) in
  let p2_2 = Rects.of_n_bound rect_dim2 p2_dim (- 2.) in
  let expected2 = Series.of_segment_array Rects.equal
    [| Series.mk_segment 0 p2_0;
       Series.mk_segment 1 p2_1;
       Series.mk_segment 3 p2_2;
       Series.mk_segment 4 p2_1;
       Series.mk_segment 6 p2_0 |] in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps2)
    expected2 v2

let test_identify_or _ =
  let samples1 = [| 0.; 1.; 0.; 1.; 0.; 1.; 0.|] in
  let samples2 = [| 1.; 0.; 1.; 0.; 1.; 0.; 1.|] in
  let samples3 = [| 0.; 0.; 1.; 1.; 0.; 0.; 0.|] in
  let samples4 = [| 0.; 0.; 0.; 2.; 2.; (-1.); (-1.) |] in
  let series1 = Series.of_sample_array (=) samples1 in
  let series2 = Series.of_sample_array (=) samples2 in
  let series3 = Series.of_sample_array (=) samples3 in
  let series4 = Series.of_sample_array (=) samples4 in
  let signal1 = Signal.mk [| series1; series2 |] in
  let signal2 = Signal.mk [| series3; series4 |] in
  let x0 = mk_var 0 in
  let x1 = mk_var 1 in
  let p0 = mk_param 0 in
  let f = Or [| Leq (x0, SParam p0);  Leq (x1, SParam p0) |] in
  let ps = PI.mk_pspace f in
  let rect_dim = PI.pspace_rect_dim ps in
  let p0_dim = PI.pdata_dim (PI.pspace_data ps p0) in
  let p0_0 = Rects.of_n_bound rect_dim p0_dim 0. in
  let p0_1 = Rects.of_n_bound rect_dim p0_dim 1. in
  let p0_n1 = Rects.of_n_bound rect_dim p0_dim (- 1.) in
  let _, v1 = PI.identify f signal1 in
  let expected1 = Series.of_segment (Series.mk_segment 6 p0_0) in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    expected1 v1;
  let _, v2 = PI.identify f signal2 in
  let expected2 = Series.of_segment_array Rects.equal
    [| Series.mk_segment 2 p0_0;
       Series.mk_segment 3 p0_1;
       Series.mk_segment 4 p0_0;
       Series.mk_segment 6 p0_n1 |]
  in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    expected2 v2

let test_identify_and _ =
  let samples1 = [| 0.; 1.; 0.; 1.; 0.; 1.; 0.|] in
  let samples2 = [| 1.; 0.; 1.; 0.; 1.; 0.; 1.|] in
  let samples3 = [| 0.; 0.; 1.; 1.; 0.; 0.; 0.|] in
  let samples4 = [| 0.; 0.; 0.; 2.; 2.; (-1.); (-1.) |] in
  let series1 = Series.of_sample_array (=) samples1 in
  let series2 = Series.of_sample_array (=) samples2 in
  let series3 = Series.of_sample_array (=) samples3 in
  let series4 = Series.of_sample_array (=) samples4 in
  let signal1 = Signal.mk [| series1; series2 |] in
  let signal2 = Signal.mk [| series3; series4 |] in
  let x0 = mk_var 0 in
  let x1 = mk_var 1 in
  let p0 = mk_param 0 in
  let f = And [| Leq (x0, SParam p0);  Leq (x1, SParam p0) |] in
  let ps = PI.mk_pspace f in
  let rect_dim = PI.pspace_rect_dim ps in
  let p0_dim = PI.pdata_dim (PI.pspace_data ps p0) in
  let p0_0 = Rects.of_n_bound rect_dim p0_dim 0. in
  let p0_1 = Rects.of_n_bound rect_dim p0_dim 1. in
  let p0_2 = Rects.of_n_bound rect_dim p0_dim 2. in
  let _, v1 = PI.identify f signal1 in
  let expected1 = Series.of_segment (Series.mk_segment 6 p0_1) in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    expected1 v1;
  let _, v2 = PI.identify f signal2 in
  let expected2 = Series.of_segment_array Rects.equal
    [| Series.mk_segment 1 p0_0;
       Series.mk_segment 2 p0_1;
       Series.mk_segment 4 p0_2;
       Series.mk_segment 6 p0_0 |]
  in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    expected2 v2

let test_identify_fg _ =
  let samples1 = [| 2.; 2.; 1.; 1.; 0.; 0.; 1.; 1.; 2.; 2.; |] in
  let series1 = Series.of_sample_array (=) samples1 in
  let signal1 = Signal.mk [| series1 |] in
  let x0 = mk_var 0 in
  let p0 = mk_param 0 in
  let f1 = F (mk_intvl (TLit 1) (TLit 3), Leq (x0, SParam p0)) in
  let f2 = G (mk_intvl (TLit 0) (TLit max_int), f1) in
  (* For f2, the parameter space is the same. *)
  let ps = PI.mk_pspace f1 in
  let rect_dim = PI.pspace_rect_dim ps in
  let p0_dim = PI.pdata_dim (PI.pspace_data ps p0) in
  let empty = Rects.mk_empty rect_dim in
  let p0_0 = Rects.of_n_bound rect_dim p0_dim 0. in
  let p0_1 = Rects.of_n_bound rect_dim p0_dim 1. in
  let p0_2 = Rects.of_n_bound rect_dim p0_dim 2. in
  (* NOTE: 'Eventually' corresponds to a union of rectangles and this corresponds to
     the minimum of coordinates. *)
  let _, v1 = PI.identify f1 signal1 in
  let _, v2 = PI.identify f2 signal1 in
  let expected1 = Series.of_segment_array Rects.equal
    [| Series.mk_segment 0 p0_1;
       Series.mk_segment 4 p0_0;
       Series.mk_segment 6 p0_1;
       Series.mk_segment 8 p0_2;
       Series.mk_segment 9 empty |]
  in
  let expected2 = Series.mk_const 9 empty in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    expected1
    v1;
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps)
    expected2
    v2

(** Tests that [true U e] is equivalent to [F e] and [false R e] - to [G e]. *)
let test_identify_ur_fg _ =
  let intvl = mk_intvl (TLit 0) (TLit max_int) in
  let samples1 = [| 2.; 2.; 1.; 1.; 0.; 0.; 1.; 1.; 2.; 2.; |] in
  let series1 = Series.of_sample_array (=) samples1 in
  let signal1 = Signal.mk [| series1 |] in
  let x0 = mk_var 0 in
  let p0 = mk_param 0 in
  let f1 = U (intvl, True, Leq (x0, SParam p0)) in
  let f2 = R (intvl, False, f1) in
  let f1_f = F (intvl, Leq (x0, SParam p0)) in
  let f2_g = G (intvl, f1_f) in
  let ps1, v1 = PI.identify f1 signal1 in
  (* Assuming f1_f has the same parameter space as f1. *)
  let _, expected1 = PI.identify f1_f signal1 in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps1)
    expected1
    v1;
  let ps2, v2 = PI.identify f2 signal1 in
  (* Assuming f2_g has the same parameter space as f2. *)
  let _, expected2 = PI.identify f2_g signal1 in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps2)
    expected2
    v2

let test_identify_u _ =
  let series1 = Series.of_sample_array (=)
    [| 4.; 3.; 3.; 3.; 2.; 2.; 1.; 1.; 1.; 0.; 0. |] in
  let series2 = Series.of_sample_array (=)
    [| 0.; 0.; 1.; 1.; 1.; 2.; 2.; 2.; 3.; 4.; 4. |] in
  let signal1 = Signal.mk [| series1; series2 |] in
  let p0 = mk_param 0 in
  let f1 = PstlU.parse_string "(U (0 inf) (>= x0 p0) (>= x1 p0))" in
  let ps1, v1 = PI.identify f1 signal1 in
  let rect_dim1 = PI.pspace_rect_dim ps1 in
  let p0_dim1 = PI.pdata_dim (PI.pspace_data ps1 p0) in
  let p0_2 = Rects.of_n_bound rect_dim1 p0_dim1 (- 2.) in
  let p0_3 = Rects.of_n_bound rect_dim1 p0_dim1 (- 3.) in
  let p0_4 = Rects.of_n_bound rect_dim1 p0_dim1 (- 4.) in
  let expected1 = Series.of_segment_array Rects.equal
    [| Series.mk_segment 7 p0_2;
       Series.mk_segment 8 p0_3;
       Series.mk_segment 10 p0_4 |]
  in
  assert_equal
    ~cmp:(Series.equal Rects.equal)
    ~printer:(PI.series_to_string ps1)
    expected1
    v1

let test_read_file _ =
  let signal = Signal.read_file_floats "data/simple_signal1.txt" in
  let expected0 = Series.of_sample_array (=) [| 1.; 2.; 3. |] in
  let expected1 = Series.of_sample_array (=) [| -0.5; -0.25; -0.125 |] in
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_float)
    expected0
    (Signal.get signal 0);
  assert_equal
    ~cmp:(Series.equal (=))
    ~printer:(Series.to_string pp_print_float)
    expected1
    (Signal.get signal 1)

let tests = "tests" >:::
  [ "test_parse_formula" >:: test_parse_formula;
    "test_format_formula" >:: test_format_formula;
    "test_mk_nnf" >:: test_mk_nnf;
    "test_push_temporal" >:: test_push_temporal;
    "test_series_array" >:: test_series_array;
    "test_point_compare" >:: test_point_compare;
    "test_of_points" >:: test_of_points;
    "test_leq" >:: test_leq;
    "test_union" >:: test_union;
    "test_union_cmp" >:: test_union_cmp;
    "test_intersect" >:: test_intersect;
    "test_backshift_int_max" >:: test_backshift_int_max;
    "test_backshift_union" >:: test_backshift_union;
    "test_until_int_max" >:: test_until_int_max;
    "test_format_param_bounds" >:: test_format_param_bounds;
    "test_identify_trivial" >:: test_identify_trivial;
    "test_identify_param_ineq" >:: test_identify_param_ineq;
    "test_identify_or" >:: test_identify_or;
    "test_identify_and" >:: test_identify_and;
    "test_identify_fg" >:: test_identify_fg;
    "test_identify_ur_fg" >:: test_identify_ur_fg;
    "test_identify_u" >:: test_identify_u;
    "test_read_file" >:: test_read_file ]

let _ =
  run_test_tt_main tests
