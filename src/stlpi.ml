open Format

module Pstl = Pstl.Make(Numbers.Float)
module Rects = URects.Make(Numbers.Float)
module PI = ParamIdent.Make(Numbers.Float)

type arg_formula_t =
  | FormulaString of string
  | FormulaFile of string

type output_t =
  | OutputAt0
  | OutputSeries

type args_t = {
  mutable args_signal_file: string option;
  mutable args_formula: arg_formula_t option;
  mutable args_read_lines: int option;
  mutable args_skip_lines: int option;
  mutable args_print_counters: bool;
  mutable args_print_formula: bool;
  mutable args_output: output_t;
  mutable args_transforms: (Pstl.formula_t -> Pstl.formula_t) array
}

let mk_empty_args () =
  { args_signal_file = None;
    args_formula = None;
    args_read_lines = None;
    args_skip_lines = None;
    args_print_counters = false;
    args_print_formula = false;
    args_output = OutputAt0;
    args_transforms = [| Pstl.mk_nnf; Pstl.push_temporal |] }

let args_signal_file args = 
  args.args_signal_file

let args_require_signal_file args =
  match args_signal_file args with
  | Some file -> file
  | None -> invalid_arg "Signal file should be given."

let args_formula args =
  args.args_formula

let args_require_formula args =
  match args_formula args with
  | Some formula -> formula
  | None -> invalid_arg "Formula should be given."
  
let args_read_lines args = 
  args.args_read_lines

let args_skip_lines args =
  args.args_skip_lines

let args_print_counters args = 
  args.args_print_counters

let args_print_formula args =
  args.args_print_formula

let args_output args =
  args.args_output

let args_transforms args =
  args.args_transforms

let args_set_formula_string args formula =
  args.args_formula <- Some (FormulaString formula)

let args_set_formula_file args file =
  args.args_formula <- Some (FormulaFile file)

let args_set_signal_file args file =
  args.args_signal_file <- Some file

let args_set_read_lines args n =
  args.args_read_lines <- Some n

let args_set_skip_lines args n =
  args.args_skip_lines <- Some n

let args_set_print_counters args b =
  args.args_print_counters <- b

let args_set_print_formula args b =
  args.args_print_formula <- b

let args_set_output args o =
  args.args_output <- o

let args_set_rewrite args b =
  if b then
    args.args_transforms <- [| Pstl.mk_nnf; Pstl.push_temporal |]
  else
    args.args_transforms <- [| Pstl.mk_nnf |]

let arg_spec args =
  [ ("-formula", Arg.String (args_set_formula_string args),
      "<formula>  Identify parameters in the given STL formula.");
    ("-f", Arg.String (args_set_formula_string args),
      " Same as -formula.");
    ("-formula-file", Arg.String (args_set_formula_string args),
      "<filename>  Read formula from the given file.");
    ("-ff", Arg.String (args_set_formula_file args),
      " Same as -formula-file.");
    ("-lines", Arg.Int (args_set_read_lines args),
      "<number>  Read at most the given number of lines from the signal file \
       (but at least 1).");
    ("-l", Arg.Int (args_set_read_lines args),
      " Same as -lines.");
    ("-skip-lines", Arg.Int (args_set_skip_lines args),
      "<number>  When reading the signal file, first skip the given number of \
       lines.");
    ("-ls", Arg.Int (args_set_skip_lines args),
      " Same as -lines-skip.");
    ("-pc", Arg.Unit (fun () -> args_set_print_counters args true),
      " Print performance counters to stderr.");
    ("-pf", Arg.Unit (fun () -> args_set_print_formula args true),
      " Echo the formula (after all transforms) to stderr.");
    ("-o0", Arg.Unit (fun () -> args_set_output args OutputAt0),
      " Output validity domain at time 0 (default)");
    ("-os", Arg.Unit (fun () -> args_set_output args OutputSeries),
      " Output full validity signal (Warning: this may be a few megabytes of data).");
    ("-rewrite", Arg.Bool (args_set_rewrite args),
      "<true|false>  Rewrite the formula by pushing temporal operators towards \
       the leaves.");
    ("-r", Arg.Bool (args_set_rewrite args),
      " same as -rewrite.") ]

let anon_spec args v =
  args_set_signal_file args v

let usage =
  "Usage:\n\
   stlpi [options] -f <formula> <signal file>\n\
   stlpi [options] -ff <formula file> <signal file>\n\
   You can write 'stdin' instead of a file name to read from standard input\n\
   options are:"  

let parse_args () =
  let args = mk_empty_args () in
  Arg.parse (arg_spec args) (anon_spec args) usage;
  args

let () =
  let args = parse_args () in
  let formula =
    match args_require_formula args with
    | FormulaString s -> PstlU.parse_string s
    | FormulaFile file_name -> PstlU.parse_file ~stdin_name:"stdin" file_name
  in
  let formula = Array.fold_left
    (fun acc transform -> transform acc)
    formula
    (args_transforms args)
  in
  if args_print_formula args then
    eprintf "%a@." Pstl.format_formula_sexp formula
  else ();
  let signal =
    Signal.read_file_floats
      ~stdin_name:"stdin"
      ?read_n:(args_read_lines args)
      ?skip_n:(args_skip_lines args)
      (args_require_signal_file args)
  in
  let t_start_identify = Unix.gettimeofday () in
  let ps, vseries = PI.identify formula signal in
  let t_end_identify = Unix.gettimeofday () in
  Counters.fset Counters.identify_time (t_end_identify -. t_start_identify);
  if args_print_counters args then
    eprintf "%a@." Counters.format_counters ()
  else ();
  match args_output args with
  | OutputAt0 ->
      printf "%a@." (PI.format_rects ps) (Series.at_t vseries 0)
  | OutputSeries ->
      printf "%a@."
        (PI.format_series ~print_sep:(fun fmt -> pp_print_newline fmt ()) ps)
        vseries



