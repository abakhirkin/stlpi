open U
open Format

module DynArray = Batteries.DynArray

type 'a signal_t = {
  sig_comps: 'a Series.t array
}

let mk comps =
  let c_len = Array.length comps in
  check_arg (c_len > 0) "Signal should have at least one component.";
  let t_len = Series.time_len comps.(0) in
  for i = 1 to c_len - 1 do
    check_arg
      (Series.time_len comps.(i) = t_len)
      "Signal components should have the same duration."
  done;
  { sig_comps = comps }

let get s i =
  s.sig_comps.(i)

let comp_len s =
  Array.length s.sig_comps

let time_len s =
  (* NOTE: Signal always has at least one component. *)
  Series.time_len (get s 0)

let read_chan ?(read_n=max_int) ?(skip_n=0) num_of_string num_eq chan =
  (* TODO: Maybe count the number of lines first. *)
  let whitespace_re = Str.regexp "[ \t]+" in
  let skip_line () =
    let _ = input_line chan in ()
  in
  let read_line () =
    let line = input_line chan in
    let strings = Str.split whitespace_re line in
    Array.map num_of_string (Array.of_list strings)
  in
  for i = 0 to skip_n - 1 do skip_line () done;
  let numbers = read_line () in
  let arrays_n = Array.length numbers in
  check_arg (arrays_n > 0) "The first line should have at least one column.";
  let sample_arrays = Array.map
    (fun number ->
      let samples = DynArray.create () in
      DynArray.add samples number;
      samples)
    numbers
  in
  let rec read_chan_rec n =
    if n <= 0 then
      ()
    else begin
      let numbers = read_line () in
      (* NOTE: This is mostly to skip trailing empty lines. Skipping empty lines
         in the middle does not hurt either. *)
      if (Array.length numbers > 0) then begin
        check_arg
          (Array.length numbers = arrays_n)
          "Every line should have the same number of columns.";
        Array.iter2
          (fun samples number -> DynArray.add samples number)
          sample_arrays
          numbers;
      end else ();
      read_chan_rec (n - 1)
    end
  in
    begin try
      read_chan_rec (read_n - 1)
    with
    | End_of_file -> ()
    end;
    mk (Array.map (Series.of_sample_dynarray num_eq) sample_arrays)

let read_chan_floats ?(skip_n=0) ?(read_n=max_int) chan =
  read_chan ~read_n:read_n ~skip_n:skip_n float_of_string (=)

let read_file ?stdin_name ?(skip_n=0) ?(read_n=max_int) num_of_string num_eq file_name =
  FileU.with_in_file_chan
    ?stdin_name:stdin_name
    (read_chan ~read_n:read_n ~skip_n:skip_n num_of_string num_eq)
    file_name

let read_file_floats ?stdin_name ?(read_n=max_int) ?(skip_n=0) file_name =
  read_file
    ?stdin_name:stdin_name ~read_n:read_n ~skip_n:skip_n float_of_string (=) file_name
