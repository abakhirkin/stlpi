#load "str.cma"

module SysU = struct

  let escape_quotes str =
    Str.global_replace (Str.regexp_string "\"") "\\\"" str

  let quote str =
    "\"" ^ str ^ "\""

  let cmdline_of_list cmd_list =    
    String.concat
      " "
      (List.map
        (* TODO: Do not quote when not needed. *)
        (fun cmd -> (quote (escape_quotes cmd)))
        cmd_list)

  let run_cmdline ?(echo=true) cmdline =
    if echo then Format.printf "%s@." cmdline else ();
    match Sys.command cmdline with
    | 0 -> None
    | err -> Some err

  let arg_list () =
    match Array.to_list Sys.argv with
    | [] | [ _ ] -> []
    | name :: args -> args    
end

module Ocb = struct

  type ocb_t = {
    ocb_bin: string;
    ocb_flags: string list;
    ocb_default_args: string list;
    ocb_aliases: (string * string list) list
  }

  let run_ocb ocb args =
    SysU.run_cmdline (SysU.cmdline_of_list (ocb.ocb_bin :: ocb.ocb_flags @ args))  

  let wrap_ocb ocb =
    let orig_args =
      match SysU.arg_list () with
      | [] -> ocb.ocb_default_args
      | args -> args
    in
    let rev_args = List.fold_left
      (fun acc arg ->
        try
          (List.rev (List.assoc arg ocb.ocb_aliases)) @ acc
        with
        | Not_found -> arg :: acc)
      []
      orig_args
    in
    run_ocb ocb (List.rev rev_args)
end
