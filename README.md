Requirements
------------

Linux, OCaml, Opam.
If you're not sure how to install OCaml and Opam, see section [How to Install 
OCaml and Opam](#how-to-install-ocaml-and-opam) in the end of this document.

Setup
-----

1. Install dependencies via Opam:
```bash
opam install ocamlbuild ocamlfind batteries menhir ounit
```

2. Go to the tool folder and build the native binaries:
```bash
./build.ml
```
This will produce `stlpi.native` which is the main executable and `tests.native`
which contains OUnit tests.

3. If you wish to build the bytecode binaries instead, run
```bash
./build.ml byte
```
**Please do not use the bytecode binaries to measure performance.**

The script `build.ml` is a thin wrapper of ocamlbuild. If you wish, you may run
ocamlbuild directly.

How to Install OCaml and Opam
-----------------------------

We suggest you do not install OCaml from your distro's repository and use Opam 
instead. You will need Opam anyway. If you are not sure how to install Opam, do 
the following

1. Install gcc, make, m4, and patch.

2. Go to https://github.com/ocaml/opam/releases/latest and download the binary 
for your system.

3. Rename the binary to `opam`, make it executable, and put it somewhere in the 
`$PATH`.

4. Run `opam init --comp 4.04.0`. This will install OCaml 4.04 for the current 
user.  When prompted, allow the script to modify the config files. After it 
finishes, run ``eval `opam config env` `` as suggested.
