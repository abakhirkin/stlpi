set terminal postscript eps size 10cm,3.3cm
set output 'fig-square-wave.eps'
set xrange [0:2750]
plot 'square.txt' with histeps lt rgb 'black' not
