set terminal postscript eps size 10cm,2cm color
set xrange [0:34]
set yrange [-0.25:2.25]
set ytics 0,1,2
set output 'fig-square-example-slides-x.eps'
plot 'esquares.txt' u 1 with histeps title 'x' lw 2 lc rgb "red"
set output 'fig-square-example-slides-y.eps'
plot 'esquares.txt' u 2 with histeps title 'y' lw 2 lc rgb "blue"
