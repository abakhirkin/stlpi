set terminal postscript eps size 10cm,2cm
set xrange [0:34]
set yrange [-0.25:2.25]
set ytics 0,1,2
set output 'fig-square-example-x.eps'
plot 'esquares.txt' u 1 with histeps title 'x'
set output 'fig-square-example-y.eps'
plot 'esquares.txt' u 2 with histeps title 'y'
