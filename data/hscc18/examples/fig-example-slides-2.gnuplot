set terminal postscript eps size 10cm,2cm color
set xrange [0:20]
set yrange [-0.5:4.5]
set ytics 0,1,4
set xtics 1,2,19
set output 'fig-example-slides-2.eps'
plot 'example-slides-2.txt' u 1 with steps title 'x' lw 2 lc rgb "red"
