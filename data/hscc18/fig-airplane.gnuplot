set terminal postscript eps size 10cm,4.5cm
set output 'fig-airplane.eps'
set xrange [0:2750]
plot 'airplane.txt' u 2 w histeps title 'cmd - resp', 'airplane.txt' u 3 w histeps title 'resp - cmd'
